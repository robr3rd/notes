# Apache - Log Analysis

## Get all **dates** in an Apache log file
> Given `[07/Aug/2016:17:02:07` it this will give `[07`.

```bash
cat /var/log/path/to/access.log | awk '{print $4}' | awk -F/ '{print $1}' | uniq
```

Note that by swapping the `$1` in the last `awk` command with `$2` you will be able to get the months.



## Get all **times** in an Apache log file
> Given the same string, this will yield `17`

```bash
cat /var/log/path/to/access.log | awk '{print $4}' | awk -F/ '{print $3}' | awk -F: '{print $2}' | uniq
```

Note that by swapping out the `$x` in the final `awk` command you can get differing results:

- `$1` yields `2016` (the year)
- `$2` yields `17` (the hour)
- `$3` yields `02` (the minute)
- `$4` yields `07` (the second)
