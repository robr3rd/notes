# Grep
## File filters
What if you're running a recursive `grep` on a folder, but only want it to run on _files with specific names_? This allows you to do that.

```shell
grep -ri 'needle' ./haystack # A typical command

grep -ri --include \*.sh 'needle' ./haystack # Only for `*.sh` files

grep -ri --include \*.js --exclude ./node_modules\* 'needle' ./haystack

grep -ri --include \*.php --include \*.js --exclude ./vendor\* --exclude ./node_modules\* 'needle' ./haystack
```
