# Zsh

## Debug Zsh startup time
Add this line to `~/.zshrc` and run `zprof` in a new shell for debug timings to identify the source of slowdowns when initializing a new shell.

```shell
zmodload zsh/zprof
```


## Detailed script "benchmarking"
```shell
# Customize 'time' command (Zsh has its own implementatino of 'time' and offers many more options vs. Bash)
TIMEFMT='%J   %U  user %S system %P cpu %*E total'$'\n'\
'avg shared (code):         %X KB'$'\n'\
'avg unshared (data/stack): %D KB'$'\n'\
'total (sum):               %K KB'$'\n'\
'max memory:                %M MB'$'\n'\
'page faults from disk:     %F'$'\n'\
'other page faults:         %R'
```