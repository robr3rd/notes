#!/usr/bin/env bash
# Convert a unix timestamp into a "normal" timestamp.
#
# Note: If on macOS, it may be necessary to install GNU's `date`.
# This is done via `brew install coreutils` and replacing `date` with `gdate`.

# Current
date -d @$(date -u +%s)

# Specific
date -d @1234567890

# Specific (arg)
# date -d @${1} # Commented out to avoid a failure when "just running" this
