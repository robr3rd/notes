#!/usr/bin/env bash

has_notified=false

while $has_notified == false
do
	monitor=$(false) # what to monitor (must return true/false)

	if [ $monitor ]; then
		echo "running at $(date)"
	else
		mail -s "Subject" robr3rd@example.com <<< "Body text!"
		echo "Finished running at or just before $(date)"
	fi
done
