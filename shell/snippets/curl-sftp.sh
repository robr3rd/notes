#! /usr/bin/env bash
# Example: curl-sftp [local_file] [remote_user] [remote_password] [remote_host] [remote_destination]

# Since cURL makes the request over HTTP, special characters (according to the HTTP spec) will cause the process to fail
sftp_password=${3//%/%25} # Escape percent signs (%) -- NOTE: This one MUST BE DONE FIRST!!! Otherwise, since HTTP character codes are "%##" we would end up replacing all of THOSE percent signs TOO!
sftp_password=${sftp_password//\@/%40} # Escape "at" symbols (@)

# Execute the cURL request
curl -k -T "$1" sftp://"$2":$sftp_password@"$4"/"$5"