#!/usr/bin/env bash
# In order, this:
# 1. Lists all branches that have been merged.
# 2. Ensures that 'master' is excluded from that list.
# 3. Pass each item in as the '<branchname>' argument to 'git branch -d'.

git branch --merged | grep -v master | xargs git branch -d
