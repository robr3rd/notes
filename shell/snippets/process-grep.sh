#!/usr/bin/env bash
#
# Process Grep
# `grep` the contents of `ps waux`
# This script automatically excludes any traces of itself (and, consequently, `grep`)
ps waux | grep $1 | grep -v grep | grep -v "$0"

# Alternate run mode:
# As an alternative to running this as a full-fledged Bash script, you could
# ...instead add the below to your shell config file (e.g. `~/.zshrc~):
# function psg { ps waux | grep $1 | grep -v grep | grep -v "$0"; }
