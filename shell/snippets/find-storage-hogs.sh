#! /usr/bin/env bash
#
# List all directories (recursively) that are occupying >500MB of disk space (sorted with largest on top)

# Linux
du -k ~/* | awk '$1 > 500000' | sort -nr

# Mac
# gdu -k --exclude=Library ~/* | awk '$1 > 500000' | sort -nr
