#!/usr/bin/env bash
# Benchmark any shell command.
#
# Usage:
# Replace 'zsh -i -c' with whatever you choose.
#
# Example:
# Below is an example of how to use this snippet. Here, we invoke the Zsh shell to see how long it takes to finish the startup on it.
#
# Example Output:
#         6.60 real         3.78 user         1.68 sys
#         5.20 real         3.62 user         1.15 sys
#         5.28 real         3.60 user         1.16 sys
#         5.28 real         3.66 user         1.16 sys
#         5.34 real         3.71 user         1.19 sys
#         5.21 real         3.60 user         1.15 sys
#         5.17 real         3.61 user         1.14 sys
#         5.33 real         3.72 user         1.18 sys
#         5.24 real         3.64 user         1.16 sys
#         5.17 real         3.59 user         1.15 sys

for i in $(seq 1 10); do /usr/bin/time zsh -i -c exit; done
