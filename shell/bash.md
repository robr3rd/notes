# Bash
(a.k.a. "Things applicable to practically all Bourne-derived shells")

Nearly everything covered here can be found in: http://linuxsig.org/files/bash_scripting.html



## Built-in Shell variables
(see: http://superuser.com/a/247131 as well as the [POSIX Standard](http://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html#tag_18_05_02))

```shell
$#    Stores the number of command-line arguments that
      were passed to the shell program.
$?    Stores the exit value of the last command that was
      executed.
$0    Stores the first word of the entered command (the
				name of the shell program).
$*    Stores all the arguments that were entered on the
      command line ($1 $2 ...).
"$@"  Stores all the arguments that were entered
      on the command line, individually quoted ("$1" "$2" ...).
```


## Indirect Variable References (i.e. "Variables Variables")
Bash support dynamic variable names, a la PHP's "variable variables".

What this amounts to is Bash having the ability to create and update variables whose names are equal to that of _another_ variable's value.

There are a few examples on the official documentation page: http://tldp.org/LDP/abs/html/ivr.html#IVRREF



## Floating Point Arithmetic
In short: Bash doesn't support this. (Note: `zsh` does, so you could run `echo 'print $(( 1/3. ))' | zsh` in Bash to "cheat" so long as Zsh is installed.)

Because of this, you could instead use `echo $(( 100 * 1 / 3 )) | sed 's/..$/.&/'` or:

```shell
IMG_WIDTH=100
IMG2_WIDTH=3
RESULT=$((${IMG_WIDTH}00/$IMG2_WIDTH))
echo "${RESULT:0:-2}.${RESULT: -2}"
33.33
```

...or any number of other solutions detailed on this page (including the above two): https://stackoverflow.com/questions/12722095/how-do-i-use-floating-point-division-in-bash


## Benchmarking
When benchmarking `bash` scripts, it is almost certainly easiest to use the `time` command. It isn't suitable for all types of scripts, but will suffice for most situations.

```shell
time sleep 1
```



## Run in Background
To run a Bash script in the background, it must first be understood that there is a different between "running in the background for your user session" and "running detached on its own in the background".


### User Background
Running in the "user background" allows continued use of the calling Bash session, but if that session ends for any reason (i.e. the user logging out), then the command is interrupted immediately.

```shell
[some cmd] &

wget http://example.com/foo.bar & # This will return you to `stdin` thus still allowing use of the active Bash session while the command completes
```


### Detached Background
A command that has been "detached" is a command that does not have a particular user that is marked as the "owner" of the process; in fact, it's best to just think of things as "processes" to really understand what's happening here.

A typical command is a process that is "owned" by a user, so when that user goes away, their processes do, too.

There are two common ways to spawn detached/disowned processes:

```shell
# Method 1
[some cmd] & disown # Executes the command, moves it to the "user background" to return the user to `stdin`, then runs the `disown` command, which frees the process of user ownership

# Method 2
[some cmd] # After running the command, hit `Ctrl + z` to move the command to the "user background" (the same as a trailing `&`)...
disown # ...then explicitly detach it from the user that spawned it

# Method 3 (preferred)
nohup [some cmd] & # Essentially, this is equivalent to Method 1 and is "the right way to do it". It also logs all output that would otherwise normally be dumped to `stdout` into `./nohup.out` releative to the directory from which the `nohup` command was executed

# Note on 'nohup'
nohup [some cmd] > output.txt & # 'nohup' will create a file called './nohup.out' by default. This allows you to pick your own target.
```

### Find in Background
```shell
jobs -l
```



## Brackets (single vs double)
> [ServerFault answer](https://serverfault.com/a/754634) (which, in turn, cites [this](http://www.thegeekstuff.com/2010/06/bash-conditional-expression/) as _its_ source)
```
Single Bracket i.e. []
For comparison ==, !=, <, and > and should be used and for numeric comparison eq, ne,lt and gt should be used.

Enhanced Brackets i.e. [[]]

We use single brackets to enclose a conditional expression, but bash allows double brackets which serves as an enhanced version of the single-bracket syntax for comparison ==, !=, <, and > can use literally.

[ is a synonym for test command. Even if it is built in to the shell it creates a new process.
[[ is a new improved version of it, which is a keyword, not a program.
[[ is understood by Korn and Bash.
This is the shell globbing feature, which will be supported only when you use [[ (double brackets) and therefore many arguments need not be quoted.
```


> [ServerFault answer](https://serverfault.com/a/705144)
```
Some differences on Bash 4.3.11:

POSIX vs Bash extension:
[ is POSIX
[[ is a Bash extension
regular command vs magic
[ is just a regular command with a weird name.

] is just an argument of [ that prevents further arguments from being used.

Ubuntu 16.04 actually has an executable for it at /usr/bin/[ provided by coreutils, but the bash built-in version takes precedence.

Nothing is altered in the way that Bash parses the command.

In particular, < is redirection, && and || concatenate multiple commands, ( ) generates subshells unless escaped by \, and word expansion happens as usual.
[[ X ]] is a single construct that makes X be parsed magically. <, &&, || and () are treated specially, and word splitting rules are different.

There are also further differences like = and =~.
<
[[ a < b ]]: lexicographical comparison
[ a \< b ]: Same as above. \ required or else does redirection like for any other command. Bash extension.
I could not find a POSIX alternative to this, see: https://stackoverflow.com/questions/21294867/how-to-test-strings-for-less-than-or-equal
&& and ||
[[ a = a && b = b ]]: true, logical and
[ a = a && b = b ]: syntax error, && parsed as an AND command separator cmd1 && cmd2
[ a = a -a b = b ]: equivalent, but deprecated by POSIX
[ a = a ] && [ b = b ]: POSIX recommendation
(
[[ (a = a || a = b) && a = b ]]: false
[ ( a = a ) ]: syntax error, () is interpreted as a subshell
[ \( a = a -o a = b \) -a a = b ]: equivalent, but () is deprecated by POSIX
([ a = a ] || [ a = b ]) && [ a = b ] POSIX recommendation
word splitting
x='a b'; [[ $x = 'a b' ]]: true, quotes not needed
x='a b'; [ $x = 'a b' ]: syntax error, expands to [ a b = 'a b' ]
x='a b'; [ "$x" = 'a b' ]: equivalent
=
[[ ab = a? ]]: true, because it does pattern matching (* ? [ are magic). Does not glob expand to files in current directory.
[ ab = a? ]: a? glob expands. So may be true or false depending on the files in the current directory.
[ ab = a\? ]: false, not glob expansion
= and == are the same in both [ and [[, but == is a Bash extension.
printf 'ab' | grep -Eq 'a.': POSIX ERE equivalent
[[ ab =~ 'ab?' ]]: false, loses magic with ''
[[ ab? =~ 'ab?' ]]: true
=~
[[ ab =~ ab? ]]: true, POSIX extended regular expression match, ? does not glob expand
[ a =~ a ]: syntax error. No bash equivalent.
printf 'ab' | grep -Eq 'ab?': POSIX equivalent
Recommendation: always use [].

There are POSIX equivalents for every [[ ]] construct I've seen.

If you use [[ ]] you:

lose portability
force the reader to learn the intricacies of another bash extension. [ is just a regular command with a weird name, no special semantics are involved.
```

## Code Standards
- Standards
	- [Google's Shell Standards](https://google.github.io/styleguide/shell.xml)
- Linters
	- [Bash Linter](https://github.com/koalaman/shellcheck)
		- [Atom package](https://github.com/AtomLinter/linter-shellcheck)
- Unit Testing
	- [shUnit2](https://github.com/kward/shunit2)
