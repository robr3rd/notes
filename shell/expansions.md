# Bash - Expansions
## History Commands & Expansions
(see: [Digital Ocean's Guide to Bash History](https://www.digitalocean.com/community/tutorials/how-to-use-bash-history-commands-and-expansions-on-a-linux-vps))

### General Usage
#### Commands
- `history`
	- Outputs full history
- `history 5`
	- Outputs the last **5** history entries.
- `history | grep [command]`
	- Outputs full history, filtered by the `grep` search

#### Event Designators
- `!n`
	- Executes whichever command was #**n** in the `history` file
- `!~n`
	- Recalls what was run **n** commands ago and executes that.
	- Example:
		- `ls foo/bar` (list contents)
		- `touch foo/bar/tmp.txt` (create new file)
		- `!~2` (list contents again, now showing the `tmp.txt` file)
- `!!` == `!~1`
	- Executes the previous command again.
	- Example:
		- `touch /etc/foo.txt` (ERROR! Insuffficient permissions!)
		- `sudo !!` (SUCCESS!)
- `!ssh`
	- Perform the most recent command starting with `ssh`.
- `!?search?`
	- Perform the most recent command with the string `search` somewhere in the command (e.g. `apt-cache search`).
- `^original^repleacement^`
	- Recall the most recent command with `original` somewhere in it, replaces it with `replacement`, and re-runs it.
	- Example:
		- `cat /etc/hossts` (ERROR)
		- `^hossts^hosts^`

#### Word Designators
> After event designators, we can add a colon (:) and add on a word desginator to select a portion of the matched command.
>
> It does this by dividing the command into "words", which are defined as any chunk separated by whitespace. This allows us some interesting opportunities to interact with our command parameters.
>
> The word numbering starts at the initial command as "0", the first argument as "1", and continues on from there.
>
> For instance, we could list the contents of a directory, and then decide we want to change to it, like this:
>
> `ls /usr/share/doc/manpages`
> `cd !!:1`
>
> In cases where we are operating on the last command, we can actually compress this by removing the second "!"and the colon, like this: `cd !1`
>
> This will operate in the same way.
>
> We can refer to the first argument as "^" and the final argument as "$" if that makes sense for our purposes. These are more helpful when we use ranges instead of specific numbers. For instance, we have three ways we can get all of the arguments from a previous command into a new command:
>
> `!!:1*`
> `!!:1-$`
> `!!:*`
>
> The lone "" expands to all portions of the command being recalled other than the initial command. Similarly, we can use a number followed by "" to mean that everything after the specified word should be included.

#### Keyboard
- `up arrow key` / `ctrl`+`p`
	- m=Move backwards in history
- `down arrow key` / `ctrl`+`n`
	- Move forward in history
- `meta`+`>` / `alt`+`shift`+`.`
	- Return to current prompt (most recent)
- `meta`+`<` / `alt`+`shift`+`,`
	- Go to first line of command history (most distant)
- `ctrl`+`r`
	- Search through history interactively
	- Tip: Pressing `ctrl`+`r` a second time will move to the next instance of the same search.
- `ctrl`+`s`
	-


### Some notes on history files and multiple sessions
> By default, bash writes its history at the end of each session, overwriting the existing file with an updated version. This means that if you are logged in with multiple bash sessions, only the last one to exit will have its history saved.
>
> We can work around this by setting the histappend setting, which will append instead of overwrite the history. This may be set already, but if it is not, you can enable this by adding this line: `shopt -s histappend`.
>
> If we want to have bash immediately add commands to our history instead of waiting for the end of each session (to enable commands in one terminal to be instantly be available in another), we can also set or append the history -a command to the PROMPT_COMMAND parameter, which contains commands that are executed before each new command prompt.
>
> To do this correctly, we need to do a bit of a hack. We need to append to the history file immediately with history -a, clear the current history in our session with history -c, and then read the history file that we've appended to, back into our session history with history -r.
>
> You can do this like so: `export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"`.
>
> When you are finished, save the file and exit.
>
> To implement your changes, either log out and back in again, or source the file by typing: `source ~/.bashrc`.
