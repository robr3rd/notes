# MySQL Indices
## Forcing usage
Although MySQL's query execution plans are _usually_ the most efficient, sometimes **you just know better**.  If you find yourself in that situation, first **be _certain_ that you nkow better** because it won't happen often.  Once you've done that, just go ahead and [convince MySQL to use the right Index](http://code.openark.org/blog/mysql/7-ways-to-convince-mysql-to-use-the-right-index)  _(link: Thought process outlining 7 ways--from most extreme to more realistic--to convince MySQL to use the most efficient INDEX)_.


## Updating
When making changes to table indices in MySQL, there are often quite a few considerations that one must bear in mind.

A few examples:

- Large tables (many records)
	- If the software using the target table only performs `INSERT` and `SELECT` operations, the index can be applied on a _duplicate_ of table (see "duplicate table" methodology for an example).
	- If `UPDATE` queries are run on a table, then the index changes MUST be done on the live instance of the table.
- Small tables (few records)
	- It's generally okay to apply index changes on live tables if they are sufficiently small in size / record quantity.