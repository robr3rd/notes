# MySQL Monitoring
- [Basic Monitoring](#basic-monitoring)
- [Better Monitoring](#better-monitoring)
- [Disk Usage](#disk-usage)
- [Logs](#logs)
    - [Slow Queries](#slow-queries)


## Basic Monitoring
To monitor the MySQL process list in real-time in a manner similar to how `top` would be used, run the following command:

```shell
watch --differences "mysql -urobr3rd -p -e'SHOW FULL PROCESSLIST'"
```

This can be further improved by adding the following snippet into `.zshrc` (or equivalent config file for other shells) which will add support for arbitrary MySQL connection parameters:

```shell
# Usage: `mysqltop -urobr3rd -pSecretPassword -h example.com -P 3307`
function mysqltop() {
    MYSQL_OPTS=$@
    watch --differences "mysql $MYSQL_OPTS -e 'show full processlist'"
}
```

([source](http://naleid.com/blog/2009/06/18/poor-mans-top-for-mysql))



## Better Monitoring
```shell
watch -n1 --differences 'uptime; free -m; mysqladmin status; mysqladmin processlist;'
```



## Disk Usage
### Retrieve a sorted list of disk usage per-database per-table
```sql
SELECT 
     table_schema as `Database`, 
     table_name AS `Table`, 
     table_rows AS `Rows`,
     round(((data_length + index_length) / 1024 / 1024), 2) `Size in MB` 
FROM information_schema.TABLES 
--WHERE table_schema = 'foo' -- optionally, filter by database
--  AND table_name = 'bar' -- optionally, filter by table
ORDER BY (data_length + index_length) DESC;
```

It likely goes without saying, but the above query is friendly to, say, `WHERE table_schema = 'foo_db_name'` to get results from only one database.



## Logs
(!) FYI, tables that are considered by MySQL to be "log tables" **cannot be `TRUNCATE`d**!  As an example, if you would like to clear your slow_log, you would need to rotate the data into a backup table and (from the backup table) happily `DELETE FROM` uninterrupted.

### Slow Queries
1. Enable
    - `SET GLOBAL slow_query_log = 'On';`
1. Set expectations (in seconds)
    - `SET GLOBAL long_query_time = 10;`
1. Set log location
    - `SET GLLOBAL slow_query_log_file = '/var/log/';`
1. Test
    - In a new connection, run `SELECT SLEEP(15);` to "emulate" a long-running query.
        - "15" just has to be bigger than `long_query_time` ("10" here).