# MySQL - Scale
Various tips & tricks about operating "at scale" in production environments with production-type amounts of data.

- [Table Modifications](#table-modifications)
- [InnoDB](#innodb)



## Table Modifications
Unlike **additions** to tables which are typically very quick, **updates** are often slow, time-consuming, and sometimes just downright _crippling_ to your applications.

If you need to modify a table's schema or indices, the "best way" to do so is to create a structurally-identical NEW (empty) table, perform the operation _there_, and then migrate the data over in batches.  This reduces risk in many ways and is also often faster to execute since the modification is being done on an empty data set.

That said, there are often quite a few considerations that one must bear in mind.  A few examples:

- Large tables (i.e. many records)
	- If the software using the target table only performs `INSERT` and `SELECT` operations, the index can be applied on a _duplicate_ of table.
	- If `UPDATE` queries are run on a table, then the index changes MUST be done on the live instance of the table.
- Small tables (i.e. many records)
	- It's generally okay to apply index changes on live tables if they are sufficiently small in size / record quantity.

### Duplicate Table
```sql
USE database;
SHOW TABLE STATUS; -- Lists every table and some apporximate information (including record count, albeit not in real-time)
SHOW INDEX FROM `foo`; -- View *existing* indices on the target table

CREATE TABLE `foo_new` LIKE `foo`; -- Copies the structure but no data
CREATE INDEX `idx_foo_column1_column2` ON `foo_new` (`column1`, `column2`); -- Example action: Index change.  Another common one would be changing a column's data type

INSERT INTO `foo_new` SELECT * FROM `foo` WHERE `id` >= 1 AND `id` < 1000000; -- Proceed in controlled incremenets while monitoing server availability/resource usage, as this allows for quick and convenient shutoff + resting periods
INSERT INTO `foo_new` SELECT * FROM `foo` WHERE `id` >= 1000000 AND `id` < 2000000; -- Next batch
INSERT INTO `foo_new` SELECT * FROM `foo` WHERE `id` >= 2000000 AND `id` < 3000000; -- Next batch

-- Swap the "live" table with the "duplicate" one; keep the "live" one around temporarily in case a rollback is required
RENAME TABLE `foo` TO `foo_old`;
RENAME TABLE `foo_new` TO `foo`;
```



## InnoDB
### Converting to InnoDB "file per table"
1. `mysqldump` all databases into a single file (e.g. `data.sql`).
1. Drop all databases (except for the `mysql` schema).
1. Run `mysql -uroot -p -Ae"SET GLOBAL innodb_fast_shutdown = 0;" && systemctl stop mysqld`.
1. Modify `/etc/my.cnf` to have the content just below this list.
1. Delete `ibdata1`, `ib_logfile0`, and `ib_logfile1`.
	- Verify that `/var/lib/mysql`'s only content is the `mysql` schema.
1. `systemctl restart mysqld`
	- Note: This will re-create the 3 files that were deleted earlier at sizes of roughly 10MB, 1GB, and 1GB, respectively. This is **okay** and is by design.
1. Re-import the `data.sql` file.
	- `ibdata1` will grow, but will only contain table metadata. Each InnoDB table will exist outside of `ibdata1`.

```ini
;/etc/my.cnf

[mysqld]
innodb_file_per_table
innodb_flush_method=O_DIRECT
innodb_log_file_size=1G
innodb_buffer_pool_size=4G
;Sidenote: Whatever your set for innodb_buffer_pool_size, make sure innodb_log_file_size is 25% of innodb_buffer_pool_size.
```

#### New structure
> Suppose you have an InnoDB table named mydb.mytable. If you go into /var/lib/mysql/mydb, you will see two files representing the table
>
> `mytable.frm` (Storage Engine Header)
> `mytable.ibd` (Home of Table Data and Table Indexes for mydb.mytable)
> `ibdata1` will never contain InnoDB data and Indexes anymore.
> 
> With the `innodb_file_per_table` option in `/etc/my.cnf`, you can run `OPTIMIZE TABLE mydb.mytable` and the file `/var/lib/mysql/mydb/mytable.ibd` will actually shrink.

