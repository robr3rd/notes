# Semantic Versioning

## The Spec
> source: semver.org



## The Spec: Expansion on Development vs Stable phase
The official spec is light on details regarding how to handle active development.

The general advice is to just say "all features are Minor and all fixes are Patches" such that the only difference is "you don't care about breaking things" so Major never gets updated.

One alternate view on the subject (even so, it's only _slightly_ alternate) is the following summary of a longer piece (linked to as the souce):
> 0.0.x - Completely unstable API.
> > PATCH = Everything is a PATCH update until APIs becom
> 0.1.x - API is fairly stable and tested. Code/Quality checks are in place.e fairly stable. Every change should be considered potentially breaking.
> > MINOR = Should be seen as potentially breaking.
> > PATCH = Updates should be considered safe.
> 1.x.x - Stable release. Follow standard SemVer standards.
> > MAJOR = Incompatible API changes,  major refactor, or a major visual redesign (even if the underlying APIs are technically still compatible)
> > MINOR = Add functionality (backwards-compatible)
> > PATCH = Bug Fixes (backwards-compatible)



## Example tags (also a good test seed)
```
1.0.0
1.1.1
1.2.0
1.2.1
1.2.2
1.2.3
1.2.4
1.2.5
1.2.6
1.2.7
1.2.8
1.2.9
1.2.10
1.2.11
1.3.0
1.4.0
1.4.1
1.4.2
1.4.3
1.4.4
9.9.9-alpha
9.9.9-beta
9.9.9-rc.1
9.9.9-rc.2
9.9.9-rc.2.1
```



## List all SemVer tags in a Git repo
### Bash
> Note: `sort -V` is unsupported on macOS's version of `sort`; as such, you must, instead, install `core-utils` and use `gsort`

`git tag -l | sort -V | grep -E "^\d+\.\d+\.\d+(-[a-z]+(\.\d+)*)?"`



## Versioning frontend libraries (esp. CSS)
Doing this for frontend can be conceptually tricky, but if you just keep it simple and don’t overthink things, it’s easy.  Just ask yourself the following questions **in order**, `exit`ing at the first “yes”.

1. Does this change break any contracts?
    - ex: class name changed so downstream will references will break 
    - Outcome: Major version (breaking change) 
1. Could this change negatively impact the visual styling?
    - This can be highly subjective and is, therefore, the trickiest question to answer. Ultimately, it is a judgement call based on your own soft skills at predicting breakage due to potentially unusual implementations. 
    - Outcome: Major version (breaking change) 
1. Does this change improve the API or look & feel with no chance of causing breakage?
    - Outcome: Minor version (feature/enhancement) 
1. Does this fix the API or visual style without causing breakage?
	- Outcome: Patch version (defect/bugfix) 
