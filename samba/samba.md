# Samba

## Setup
TODO (Honestly this was years ago, so...the next time I need to do this I'll fill this in. Until then, just use this which is _probably_ what I used but regardless, in typical ArchWiki form it will probably work for any distro out there--not just Arch--so...here: [https://wiki.archlinux.org/index.php/samba](https://wiki.archlinux.org/index.php/samba).)

## Usage
### Adding shares
1. Choose files
1. `cp` to `/home/samba/` (as `sudo`...until I get groups set up more easily...like making `robert` (my user) a member of the `samba` group or something)
1. Change owner and group to `samba` (note: a sticky bit would make this easier by eliminating this last step)

### Reading shares
To read the files, just connect to the server's LAN IP address as `samba` identified by `samba`.
