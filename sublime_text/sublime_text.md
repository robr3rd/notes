# Sublime Text

## Plugins
1. AdvancedNewFile _(optional)_
1. Ansible
1. Behat
1. Color Highlight
1. DocBlockr
1, Dockerfile Syntax Highlighting
1. EditorConfig
1. Emmet
1. GotoDocumentation
1. INI
1. Jinja2 _(for Ansible templates)_
1. Laravel Blade Highlighter
1. Local History
1. Package Control
1. Pretty JSON
1. Pretty YAML
1. SideBarEnhancements
1. SublimeCodeIntel
    - Note: Read instructions on PackageControl website.
    - TL;DR = `pip3 install --upgrade --pre CodeIntel` is required.
1. SublimeLinter
    - Note #1: Must still install _each linter_ individually, e.g., `-pylint`, `-tslint`, `-stylelint`, `-shellcheck`.
    - Note #2: Each linter may have its own requirements.  Be sure to read them on the Package Control website.
1. Unquote
1. Xdebug Client _(optional; a full IDE (PhpStorm) is better for this purpose)_
1. ZSH


## Color Schemes
1. One Dark Color Scheme


## SublimeLinter packages
1. `SublimeLinter-annotations`
1. `SublimeLinter-eslint`
	- Requires: `eslint` via NPM + an `.eslintrc` file.
	- Note: If no `.eslintrc` file is found, it will continue to traverse parents.  See README for assistance to fix.
1. `SublimeLinter-json`
	- Note #1: Uses Sublime's built-in JSON Parser.
	- Note #2: Defaults "non-strict" to allow comment lines and trailing commas for ST settings file convenience.
		- See README or Package settings for how to change this behavior.
1. `SublimeLinter-php`
1. `SublimeLinter-phpcs`
1. `SublimeLinter-phplint`
1. `SublimeLinter-phpmd`
1. `SublimeLinter-pylint`
1. `SublimeLinter-shellcheck`
1. `SublimeLinter-stylelint`
	- Requires: `postcss`, `stylelint` via NPM
