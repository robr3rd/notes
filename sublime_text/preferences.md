# Sublime Text - Preferences
```json
{
	"added_words":
	[
		"Xdebug",
		"xdebug",
		"tmux"
	],
	"always_show_minimap_viewport": true,
	"bold_folder_labels": true,
	"caret_style": "phase",
	"color_scheme": "Packages/One Dark Color Scheme/One Dark.tmTheme",
	"ensure_newline_at_eof_on_save": true,
	"fade_fold_buttons": false,
	"folder_exclude_patterns":
	[
		".svn",
		".hg",
		"CVS",
		".idea"
	],
	"font_face": "Fira Code",
	"font_options":
	[
		"dlig"
	],
	"font_size": 13,
	"highlight_line": true,
	"highlight_modified_tabs": true,
	"ignored_packages":
	[
		"Markdown",
		"Vintage"
	],
	"indent_guide_options":
	[
		"draw_normal",
		"draw_active"
	],
	"line_padding_bottom": 3,
	"line_padding_top": 3,
	"overlay_scroll_bars": "disabled",
	"rulers":
	[
		80,
		120
	],
	"scroll_past_end": true,
	"show_encoding": true,
	"show_full_path": true,
	"soda_classic_tabs": true,
	"theme": "Adaptive.sublime-theme",
	"trim_trailing_white_space_on_save": true,
	"word_wrap": false
}
```