# Linux File Permissions
> content adapted from: [Getting to Know Linux File Permissions](http://www.linux.com/learn/tutorials/885268-getting-to-know-linux-file-permissions)



## Permission Groups
- User/Owner
- Group
- Others
- All



## Alpha Permission Types
Given `rw-rw-r--` (which you might see as output from `ls -l`), what does it mean?

- Read (`r`)
- Write (`w`)
- Execute (`x`)


### Practice
First, we must divide this into 3 groups of 3 characters:
1. `rw-` = The User that owns this file has read and write access
2. `rw-` = The Group(s) that own this file have read and write access
3. `r--` = Everyone else ("Other") has read access only



## Numerical Permission Types
Given `644`, `755` and so on (which you might see from input from `chmod 644 /some/file/path`), what do they mean?

- Read (`4`)
- Write (`2`)
- Execute (`1`)

Choose any combination of the above options and add the numbers together. Do this for each of the Permissions Groups (User/Owner, Group, Other) and there you have it!


### Practice
- `664` = `rw- rw- r--`
- `400` = `r-- --- ---`
- `755` = `rwx r-x r-x`
- `777` = `rwx rwx rwx`



## Files vs Directories
- Files = `-rwxr-xr-x` (`755`)
- Directories = `drwxr-xr-x` (`755`)



## Changing Permissions
- Set `chmod 000 filename`
- Modify `chmod [{u,g,o}/a][+/-][{r,w,x}] filename`
	1. First bracket (`[{u,g,o}/a]`) needs ==1 selected if `a `or >=1 if not
	1. Second bracket (`[+/-]`) needs ==1 selected
	1. Third bracket (`[{r,w,x}]`) needs >=1 selected


### Changing Permissiosn En Masse
- `chmod -R g-w /path/to/directory` = remove `w` from Group of every file and directory beneath `/path/to/directory` (including `/path/to/directory`, itself)


### Examples
- Numeric ("set")
	- `chmod 755 filename`
	- `chmod 644 filename`
	- `chmod 766 filename`
	- `chmod 400 filename`
- Alphabetical ("modify")
	- `chmod u+x filename` = add `x` to the User/Owner of `filename`
	- `chmod ug+w filename` = add `w` to BOTH the User/Owner and Group of `filename`
	- `chmod ugo-x filename` = remove `x` from User/Owner, Group of `filename` and Other
	- `chmod a-x filename` = remove `x` from all permission types (same as `ugo`)
