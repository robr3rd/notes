# Fonts

## Ligatures
Really short version: Font ligatures take `2 != 3` and turn it into `2 ≠ 3`. This is to improve readability + lessen the strain placed on the human at the human level, as it takes non-zero effort to read multiple glyphs and combine them into a single concept vs just reading a single glyph.

### Ligature Installation
You have to choose a font that support ligatures and then ensure that your application(s) support them and have had that support enabled.

The best insructions that I've found comes from the GitHub wiki for a font (with ligature support) called "Fira Code". Note that these instructions apply to all ligature-supporting fonts and _are not_ exclusive to just "Fira Code": [Fira Code - How to Install](https://github.com/tonsky/FiraCode/wiki).

## Good programming fonts
- Hack (good for editors...it's a bit "prettier")
	- Support Powerline?: Yes
- Inconsolata-g for Powerline (good for terminals...it's a bit more "literal"/"blocky" and it's easier to distinguish one character from another)
	- Supports Powerline?: Yes
- Fira Code
	- Thoughts: This weighs heavily on the "pretty" stuff and thereby sacrifices some legibility; however, it _does_ have very nice support for font ligatures!
	- Thoughts #2: This also has very complete instructions on how to enable ligatures in a wide variety of editors/IDEs/applications. As such, its "installation instructions" page could potentially be considered "fallback" instructions for other fonts wchih do not offer the same type of information.
	- Link: https://github.com/tonsky/FiraCode
