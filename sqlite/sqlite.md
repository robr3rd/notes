# SQLite
SQLite is an easy-to-use RDBMS that differs from most RDBMS solutions by NOT having explicit, separate client/server components.

This simplicity, combined with its lightweight nature, has made it an ideal choice for deployment to embedded systems, bundled inclusion in software packages (example: Android apps), and quick-and-dirty "I have structured data and need to query it in complex ways" tasks.

Additionally, its self-contained nature combined with its widespread adoption has earned it a place on the United States Library of Congress's list of Recommended Formats for long-term data archival as the only "database" format alongside XML, JSON, and CSV.

References regarding U.S LOC format:

- [SQLite's page boasting this](https://www.sqlite.org/locrsf.html)
- [Recommended Formats Statement](https://www.loc.gov/preservation/resources/rfs/)
    - [Datasets](https://www.loc.gov/preservation/resources/rfs/data.html)



## Quickly create a SQLite DB from existing data
1. Place your data in a file in a consistent format. (example: `my-file.csv`)
1. Install SQLite on your system
    - ...or use Docker: `docker run --rm -it -v "$PWD":/root/db --entrypoint bash nouchka/sqlite3`
1. Run SQLite: `sqlite3 my-dataset.db`
    1. `CREATE TABLE mytable (col1 TEXT, col2 TEXT);`
    1. `.separator ","`  <-- if not CSV, set appropriately
    1. `.import my-file.csv mytable`
    1. `SELECT * FROM mytable;`  <-- verify
1. Done!
    - You may now run `.exit` to leave the SQLite REPL.  Your data is safe & sound in `my-dataset.db`.
