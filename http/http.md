# HTTP
- [Basics](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)
	- Includes things like **Cache Control**, **Multi-language support (`MultiViews`)**, **URL vs URI**
- [State & Session Management](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_StateManagement.html)
- [Authentication](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Authentication.html)
- [Security with SSL](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_SSL.html)


## Cache Control
> The client can send a request header "Cache-control: no-cache" to tell the proxy to get a fresh copy from the original server, even thought there is a local cached copy. Unfortunately, HTTP/1.0 server does not understand this header, but uses an older request header "Pragma: no-cache". You could include both headers in your request.
> - the "Basics" link above (https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)

```http
Pragma: no-cache
Cache-Control: no-cache
```


## SSL
### Certificate Management
The key components of setting up SSL is:

1. Generate a CSR (Certificate Signing)
1. Use that CSR to create your other keys


#### Generate a CSR
> *Best practices:* No TLD in the cert -- this is to remain flexible and cut back on syntax errors.
> *References:* [GoDaddy's guide for a new CSR](https://www.godaddy.com/help/apache-generate-csr-certificate-signing-request-5269)

```bash
openssl req -new -newkey rsa:2048 -nodes -keyout [example].key -out [example].csr
```

##### Generate a CSR - the questions
1. Common Name = `*.example.com`
1. Organization = The legally-registered name for your business or the primary administrator (if as an individual).
1. Organization Unit = If applicable, enter the "doing business as" name.
1. City or Locality = Name of the city where your organization is registered/located. Do not abbreviate.
1. Country: The two-letter International Organization for Standardization (ISO) format country code for where your organization is legally registered.


## URL and URI
> Note: This section is copied verbatim from the above "Basics" resource (https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)

### URL (Uniform Resource Locator)
A URL (Uniform Resource Locator), defined in RFC 2396, is used to uniquely identify a resource over the web. URL has the following syntax:

`protocol://hostname:port/path-and-file-name`

There are 4 parts in a URL:

1. Protocol: The application-layer protocol used by the client and server, e.g., HTTP, FTP, and telnet.
1. Hostname: The DNS domain name (e.g., `www.nowhere123.com`) or IP address (e.g., `192.128.1.2`) of the server.
1. Port: The TCP port number that the server is listening for incoming requests from the clients.
1. Path-and-file-name: The name and location of the requested resource, under the server document base directory.

For example, in the URL `http://www.nowhere123.com/docs/index.html`, the communication protocol is `HTTP`; the hostname is `www.nowhere123.com`. The port number was not specified in the URL, and takes on the default number, which is TCP port `80` for HTTP [STD 2]. The path and file name for the resource to be located is `"/docs/index.html"`.

Other examples of URL are:
- `ftp://www.ftp.org/docs/test.txt`
- `mailto:user@test101.com`
- `news:soc.culture.Singapore`
- `telnet://www.nowhere123.com/`

#### Encoded URL
URL cannot contain special characters, such as blank or '~'. Special characters are encoded, in the form of %xx, where xx is the ASCII hex code. For example, `~` is encoded as `%7e`; `+` is encoded as `%2b`. A blank can be encoded as `%20` or `+`. The URL after encoding is called encoded URL.

### URI (Uniform Resource Identifier)
URI (Uniform Resource Identifier), defined in RFC3986, is more general than URL, which can even locate a fragment within a resource. The URI syntax for HTTP protocol is:

`http://host:port/path?request-parameters#nameAnchor`

The request parameters, in the form of `name=value` pairs, are separated from the URL by a `?`. The `name=value` pairs are separated by a `&`.

The `#nameAnchor` identifies a fragment within the HTML document, defined via the anchor tag `<a name="anchorName">...</a>`.

URL rewriting for session management, e.g., `...;sessionID=xxxxxx`.
