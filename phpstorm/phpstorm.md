# PhpStorm

## PhpStorm Tips (doubles as a demo of many features)
https://phpstorm.tips/


## Getting Started (especially from another editor)
https://laracasts.com/series/how-to-be-awesome-in-phpstorm


## Getting Started with PHP_CodeSniffer
https://confluence.jetbrains.com/display/PhpStorm/PHP+Code+Sniffer+in+PhpStorm


## Configuring PhpStorm Code Generation
https://murze.be/configuring-phpstorms-code-generation


## Example Setup
https://murze.be/my-current-setup-2018-edition


## Vagrantfile syntax highlighting
1. Download/`git clone` this repo: [ruby.tmbundle](https://github.com/textmate/ruby.tmbundle)
1. `Preferences` -> `Editor` -> `TextMate Bundles` -> `+` -> `Ruby`
    - Set the path to wherever you're storing the files that you just downloaded.
    - Ensure that your current `IDE Color Scheme` is matched by a similar `TextMate Color Scheme` since the latter is what will be used for these files.
1. `Preferences` -> `Editor` -> `File Types` -> `File Supported Via TextMateBundles`
    - Ensure that `Vagrantfile` is in the `Registered Patterns` section.



## Fonts
- Code
	- Primary
		- Face: Hack
		- Size: 14
		- Line spacing: 1.2
	- Fallback
		- Face: Fira Code
		- Size: 12
		- Line spacing: 1.3
- Console
    - Primary
        - Face: Hack
        - Size: 12
        - Line spacing: 1.0
    - Fallback
        - Face: Inconsolata
        - Size: 14
        - Lne spacing: 1.0


## Plugins
- .env files support
- .ignore
- ANSI Highlighter
- BashSupport
- CodeGlance
- Color Ide
- EditorConfig
- Ideolog
- IntelliJ Stylelint Plugin
- Laravel Plugin
- LiveEdit
- PostCSS support
- Puppet support
- Rainbow Brackets
- RAML Plugin for IntelliJ
- YAML/Ansible
