# Ruby
## Style Guide
- Ruby: https://github.com/bbatsov/ruby-style-guide
- Ruby on Rails: https://github.com/bbatsov/rails-style-guide



## Ruby Version Managers
### RVM
There's always `rvm`, of course, which is _literally_ **`r`uby `v`ersion `m`anager**, but...[https://github.com/rbenv/rbenv/wiki/Why-rbenv%3F](just use `rbenv` instead).

### `rbenv`
> https://github.com/rbenv/rbenv

- `rbenv install --list`
	- List all available packages
- `rbenv install [package-name]`
	- Installs that package
- `rbenv global [package-name]`
	- Set the global version of Ruby. For instance, run `ruby -v` and take note of the version number, then run `rbenv global [something-else]` and check `ruby -v` again to observe the new "default/global" Ruby version.
- To set a per-project Ruby version, simply create a file named `.ruby-version` whose contents are _only_ the name of the `rbenv` package (e.g. via `echo "2.3.1" > ~/projects/foobar/.ruby-version`).

#### `rbenv` Installation
- MacOS
	1. Install Homebrew
	1. `brew update`
	1. `brew install rbenv`
