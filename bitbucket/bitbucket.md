# Bitbucket

## Static Website Hosting (i.e. "Bitbucket Pages")
([source](https://confluence.atlassian.com/bitbucket/publishing-a-website-on-bitbucket-cloud-221449776.html))

> Quick note: If you operate in a team environment using Bitbucket Teams, you may exchange every instance of the word `ACCOUNT` with "team" since Bitbucket Teams are handled in the same way that accounts are. One such example of this would be the path of team repositories being `bitbucket.org/TEAM/repository-name` which is in the same "namespace" as `bitbucket.org/USER/repository-name`.

1. Create a Bitbucket account (if you do not already have one).
1. Take note of the username of your account.
	- If you are at all unsure, here's how an easy way find it: `https://bitbucket.org/ACCOUNT/repository-name`
1. Create a repository whose name is `ACCOUNT.bitbucket.org`. It **MUST** follow this naming convention **_verbatim_**.
1. Place an `index.html` file in the root of the repo. (See below for an example of a very basic one.)
	- `<!DOCTYPE html><html><head><title>Testing Bitbucket Static Website Hosting!</title></head><body>Testing!</body></html>`
1. Navigate to [https://ACCOUNT.bitbucket.io](https://ACCOUNT.bitbucket.io).


Some notes on what's going on here / how it works and _doesn't_ work:

- `ACCOUNT.bitbucket.io` gets translated to, "Serve `bitbucket.org/ACCOUNT/ACCOUNT.bitbucket.org` as a `DOCUMENT_ROOT` rather than through the Bitbucket Cloud web interface."
- `ACCOUNT` **must** be a valid Bitbucket User or Team; otherwise, it will not resolve.
-` bitbucket.io` seems to be how they determine when things should be served like this vs `bitbucket.org` always serving up the Bitbucket Cloud web interface.
- Naming your repository `ACCOUNT.bitbucket.io` _will not work_.
- Due to the restriction of having your Bitbucket **account name** in the repository name, this means a few things:
	1. Your account name must be compliant with the standard subdomain character restrictions (e.g. no underscores).
	1. You can only have ONE Bitbucket Static Website _per account_. This is as opposed to _per repository_ like nearly every other major Git host.
- You can work around the _one site per account_ restriction by simply using subdirectories (see below ASCII directory tree for an example of how this might look).


An example directory tree with multiple projects:
```
/
├── README.md (for people working in the repo)
├── index.html (DEFAULT - could be used to describe and list all projects...or excluded entirely)
├── project1/
|	└── index.html
├── project2/
|	└── index.html
└── project3/
	└── index.html
```
