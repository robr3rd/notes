# Supervisor
> Supervisor is a client/server system that allows its users to control a number of processes on UNIX-like operating systems.
>
> \- [Official docs](http://supervisord.org/introduction.html)

> A Queue Manager that works be defining Programs and the # of Processes, # of Retries, seconds before Timeout, wait time between Timeouts, etc. that a specific Worker should have.
> 
> Think “cron” then think “multi-threaded cron with better handling of failures AND retry thresholds.”
>
> \- My description (robr3rd)

> Supervisor is a process monitor for the Linux operating system, and will automatically restart your `queue:work` process if it fails.
>
> \- [Laravel's description](https://laravel.com/docs/5.5/queues#supervisor-configuration)



## Table of Contents
- [Install & Configure](#install-configure)
    - [Ops](#ops)
        - [Install](#install)
        - [Configure](#configure)
    - [Dev](#dev)
        - [Configure](#configure-1)
        - [Test](#test)
- [Maintain](#maintain)
    - [Deploy](#deploy)
    - [Monitor](#monitor)
    - [Retry](#retry)
    - [Cleanup](#cleanup)



## Install & Configure
### Ops
#### Install
`sudo apt-get install supervisor`

#### Configure
Next, put the below content in a file in `/etc/supervisor/conf.d/` and give it a name like `app-worker.conf`.

```ini
[program:app-worker]
process_name=%(program_name)s_%(process_num)02d
command=php /path/to/your/application/artisan queue:work redis --sleep=3 --tries=3
autostart=true
autorestart=true
user=forge
numprocs=8
redirect_stderr=true
stdout_logfile=/path/to/your/application/logs/app-worker.log
```

With the configuration file now in place, simply make Supervisor aware of its existence and have it start the new worker:
1. `sudo supervisorctl update`
1. `sudo supervisorctl start app-worker:*`



### Dev
#### Configure
Log failures:
1. (in dev): `php artisan queue:failed-table`
    - Create the migration file that makes the “failed jobs” database table, which stores a list of all failed jobs.
1. (in prod): `php artisan migrate`
    - …just run the migration.

#### Test
- All: `php artisan queue:work`
- Single Queue: `php artisan queue:work --queue=<queue_name>`
- One-off: `php artisan queue:work --once`
- Run until empty: `php artisan queue:work --stop-when-empty`



## Maintain
### Deploy
If the code for a Job changes, the Worker will need to be restarted: `php artisan queue:restart`.

### Report
Retrieve the list of failed jobs by pulling the contents of the `failed_jobs` database table.

> Example: Running `php artisan queue:failed` with `stdout` redirected to a log file every 5 minutes as a cron job would be a simple and rather portable way of accomplishing this.

### Retry
`php artisan queue:retry <all|$job_id>` where `$job_id` may be retrieved from the `failed_jobs` table.

### Cleanup
- Flush the list of ALL failed jobs with `php artisan queue:flush`.
- Forget a specific job with `php artisan queue:forget <$job_id>`.