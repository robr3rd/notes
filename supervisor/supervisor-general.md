# Table of Contents
- [TL;DR](#tldr)
- [Getting Started (Introduction)](#getting-started-introduction)
    - [Installation](#installation)
    - [Configuration](#configuration)
    - [Activation](#activation)
- [What about multiple, concurrent Processes!?](#what-about-multiple-concurrent-processes)
    - [Configuration](#configuration-1)
- [Maintenance](#maintenance)
    - [Daemon Control](#daemon-control)
    - [Program Control](#program-control)
- [Runbooks](#runbooks)
    - [Add a Program](#add-a-program)
    - [Update a Program](#update-a-program)



# TL;DR
Got a script that you want to run like a service daemon?  Use Supervisor.  That's what it's for.



# Getting Started (Introduction)
Have you ever had a script that you wanted to always be running?

Let's say you make a simple script named `monitor.sh` whose purpose is to monitor a server 24/7 and write any errors to a file.

It might even look something like this (in `/home/foobar/bin/`):

```bash
#!/usr/bin/env bash
while true; do
	exit_code="$(ping -c1 my-server.com 2>&1 > /dev/null; echo $?)"

	if [[ "$exit_code" != "0" ]]; then
		echo "$exit_code" >> error.log
	fi
done
```

That's perfect, right!?

You just run it once, walk away, and let the while true keep it going forever...right?

Wrong!

If the server is rebooted?  Killed.

If the script dies/errors out for any reason?  Killed.

This is so brittle!



With Supervisor, you simply designate what things you want to be running 24/7 (and how many concurrently, if desired), and you let it handle the rest-–everything from "start automatically on boot" to "if this dies, start it again".



Here's how YOU can get that monitor.sh script running reliably!!!

## Installation
```bash
sudo apt-get install supervisor
sudo systemctl enable supervisor
```

## Configuration
```ini
[program:my-program]                          # The name of this "Supervisor Program" (see: `supervisorctl status`)
command=/home/foobar/bin/monitor.sh           # Command to run (in this case, a script)
numprocs=1                                    # Keep only 1 Process running at a time
user=foobar                                   # Owner of the Process
autostart=true                                # Start this Program when Supervisor starts
autorestart=true                              # Restart this Process if it ever dies
redirect_stderror=true                        # Send stderr to stdout
stdout_logfile=/home/foobar/bin/monitor.log   # Send stdout [here] (includes redirected stderr)
```

## Activation
```bash
sudo supervisorctl update
sudo supervisorctl start all
```


That's it!  You now have a "daemonized" monitoring script that will "start on boot" and will be restarted if it dies!



# What about multiple, concurrent Processes!?
## Configuration
Here's an example Program with concurrent Processes (>=2 running at the same time).

```ini
[program:my-concurrent-program]
process_name=%(program_name)s_%(process_num)02d   # This _Process's_ name (`my-concurrent-program_17`)
command=php /var/www/example/cli queue:work       # Command to run (e.g., a CLI-based PHP framework control script)
numprocs=20                                       # Keep 20 concurrent Processes for this Program
user=www-data                                     # Owner of all Processes for this Program (e.g., Apache)
autostart=true
autorestart=true                                  # Restart each Process if one ever dies
redirect_stderror=true
stdout_logfile=/var/www/example/logs/worker.log
```



# Maintenance
In general, you can always start with supervisorctl help to see the full list of supported commands for your installed version.

## Daemon Control
- Status
    - `supervisorctl status`
- Reload ("restart" the daemon)
    - `supervisorctl reload`

## Program Control
- Add
    - `supervisorctl add (all|<my-program>)`
- Start
    - `supervisorctl start (all|<my-program>)`
- Stop
    - `supervisorctl stop (all|<my-program>)`
- Remove
    - `supervisorctl remove (all|<my-program>)`
- Restart
    - `supervisorctl restart (all|<name>|<name1> <name2>)`
- Update (reload config files and add/remove/restart Programs as necessary)
    - `supervisorctl update`




# Runbooks
## Add a Program
1. Create `/etc/supervisor/conf.d/<your-program>.conf`
1. `sudo supervisorctl update`

## Update a Program
1. `sudo supervisorctl update  # CAUTION: If a RUNNING Program changed it will be immediately restarted, interrupting any current processes`
