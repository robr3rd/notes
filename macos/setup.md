# MacOS - Setup

## Install applications
1. Install [CheatSheet](https://www.mediaatelier.com/CheatSheet)
	- Hold in <kbd>Cmd</kbd> (`Command`) for a few seconds while an application is open and a popover will appear. This popover has most--if not all--of that application's specific keybindings.
	- This is extraordinarily useful in day-to-day life, but also an invaluable tool for anybody new to Mac, as it helps in learning the shortcuts.
1. Install [BTT (BetterTouchTool)](https://www.boastr.net) 
	- "Supercharged" input remapping software 
	- (just purchase it if you don't already have a license...you won't regret it!)
1. Install [Amethyst](https://ianyh.com/amethyst/)
	- This is a tiling window manager, a la `xmonad`
1. Install [VitalSigns](http://www.creationalstate.com/vitalsigns/)
	- Hardware diagnostics (think `lm_sensors` but with a pretty frontend). Note: Once launched the first time, it has a widget that can be added to your notification sidebar.-
1. Install [The Unarchiver](http://unarchiver.c3.cx/unarchiver)
	- Adds support to MacOS for darn near every type of archive: ZIP, RAR, 7zip, etc.
1. Install [Karabiner-Elements](https://github.com/tekezo/Karabiner-Elements/blob/master/README.md)
	- Advanced Keyboard Remapping
	- Note that this works better for some things than others. For instance, remapping <kbd>Super/Command</kbd> and <kbd>Alt/Option</kbd> for a PC keyboard is better done using the advice below ("Remap Modifier Keys") since it is at the global scale whereas Elements seems to require that _each Modifier Keys **combination**_ be remapped as well, making it far from ideal.
1. Install [Homebrew](https://brew.sh/)
	- Package management for Mac, similar to the AUR (Arch User Repositories) on Arch Linux. (disclaimer: community-run)
	- Any time you plan to install something, you should check if there's a Homebrew package for it first just to make your life easier. Only download "the manual way" if you absolutely must.
	- Packages:
		- `brew install ansible` - Configuration Management
		- `brew install bash-completions`
		- `brew install composer` - for PHP
		- `brew install git`
		- `brew install htop-osx`
		- `brew install httpie`
		- `brew install links` - text-based web browser
		- `brew install lynx` - text-based web browser
		- `brew install meld` - 2/3-way GUI diff application
		- `brew install mysql`
		- `brew install Caskroom/cask/numi` - [Numi](https://numi.io/)
		- `brew install rbenv`
		- `brew install ruby`
		- `brew install sqlite`
		- `brew install terminal-notifier`
		- `brew install tree`
		- `brew install wget`
		- `brew install tmux`
		- `brew install zsh`
		- `brew install zsh-completions` - (check the post-install notes about `~/.zshrc` configuration)
		- `brew install zsh-syntax-highlighting` - (check the post-install notes about `~/.zshrc` configuration)



## Customize UI
- Increase the auto-hide speed of the dock: http://apple.stackexchange.com/questions/33600/how-can-i-make-auto-hide-show-for-the-dock-faster/70598#70598



## Remap Modifier Keys
MacOS ships with an out-of-the-box solution to remap the system's Modifier Keys (i.e. <kbd>Ctrl</kbd>, <kbd>Option</kbd>, <kbd>Command</kbd>, <kbd>Shift</kbd>) on a _per-keyboard_ basis.

To access this, simply plug in the desired keyboard and then navigate to: `System Preferences -> Keyboard -> Keyboard (tab) -> Modifier Keys... -> Select keyboard -> [choose your keyboard from the dropdown] -> [configure as desired] -> hit OK`.

In my case, "as desired" amounts to swapping <kbd>Alt</kbd> and <kbd>Super</kbd> so that any PC keyboard then has the same Modifier key order as a typical Apple keybaord.



## "Fix" Home/End Keys
[Map to beginning/end of line rather than beginning/end of page](http://apple.stackexchange.com/questions/16135/remap-home-and-end-to-beginning-and-end-of-line)

Insert the following contents into a file named `~/Library/KeyBindings/DefaultKeyBinding.dict`:

```
{
    "\UF729"  = moveToBeginningOfParagraph:; // home
    "\UF72B"  = moveToEndOfParagraph:; // end
    "$\UF729" = moveToBeginningOfParagraphAndModifySelection:; // shift-home
    "$\UF72B" = moveToEndOfParagraphAndModifySelection:; // shift-end
}
```
