# macOS - Homebrew
Homebrew is basically "`pacman`/`apt`/`yum` for macOS".  Said differently, it's the unofficial, de-facto, community-created-and-managed package management system for the macOS platform.

Its API is pretty simple to use, e.g., `brew install htop`, `brew upgrade htop`, or to update brew, itself, and all packages it's `brew update`.

To get more information about a package (such as a list of all versions that you have installed, where they are, and how much space they're taking up), simply run `brew info <package>`.

To switch between versions of the same package, run `brew switch <package> <version>`, e.g., if you required Node 6 LTS but the current version is Node 10 LTS and it upgrades that after a `brew update`, you'll need ot just run `brew switch node 6.<your-version>` to go back to how it was before! ([more on this](https://stackoverflow.com/questions/3987683/homebrew-install-specific-version-of-formula))
