# Email

## DMARC (SPF, DKIM)
Free DMARC monitoring services:
- https://dmarc.postmarkapp.com/
- https://www.valimail.com/dmarc-monitor/

DMARC configuration once set up:
```
$ dig _dmarc.example.com
_dmarc.example.com.	600	IN	TXT	"v=DMARC1; p=quarantine; sp=none; rua=mailto:dmarc_agg@vali.email,mailto:re+abcde12fg3h@dmarc.postmarkapp.com; ruf=mailto:postmaster@example.com"
```

How it works:
- `600` = the TTL to check for changes to these settings
- `p=quarantine` = `p`olicy
    - Which action a receiving mail server should take for messages that fail DMARC checks.
    - Options:
        - `p=none` = Do nothing; just let it hit the recipient's inbox.
        - `p=quarantine` = Send to recipient's junk/spam folder.
        - `p=reject` = Drop all non-compliant messages (users will never receive these).
- `rua` = `a`ggregate reports
    - This instructs _receiving mail servers_ to send DMARC aggregate reports (usualy an email with a gzipped XML report) to the specified email address.
    - This tends to be very noisy, so while you _could_ send them to something like `postmaster@example.com` you probably _shouldn't_; instead, you can have them sent to services like [dmarc.postmarkapp.com](dmarc.postmarkapp.com) which will receive them on your behalf, parse them, then send you (`postmaster@example.com`) a _weekly summary email_ that is representative of the aggregate reports that were sent that week.
- `ruf` = `f`orensic reports
    - This is similar to `rua` in almost all ways, except that these will include a snippet of the original offending email.
    - Generally speaking, this can just be left blank unless you know what you're doing and have a specific reason to have this enabled.

> NOTE: Compliance with both `rua` and `ruf` _are completely optional_.  Not all receiving mail servers send `rua` reports, and even _fewer_ send `ruf` reports.
