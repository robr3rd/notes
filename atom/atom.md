# Atom

## Editor (`config.cson`)
```cson
"*":
  "atom-material-ui":
    colors: {}
    fonts: {}
    tabs:
      compactTabs: true
      noTabMinWidth: true
    treeView:
      compactList: true
    ui:
      panelContrast: true
      panelShadows: true
  core:
    disabledPackages: [
      "whitespace"
    ]
    projectHome: "/Users/rrobinson/code"
    telemetryConsent: "limited"
    themes: [
      "atom-dark-ui"
      "one-dark-syntax"
    ]
    useCustomTitleBar: true
  docblockr:
    lower_case_primitives: true
    short_primitives: true
    spacer_between_sections: true
  editor:
    fontFamily: "Fira Code"
    fontSize: 12
    invisibles: {}
    preferredLineLength: 120
    scrollPastEnd: true
    showIndentGuide: true
    showInvisibles: true
    softWrap: true
    softWrapAtPreferredLineLength: true
    tabLength: 4
    zoomFontWhenCtrlScrolling: true
  "exception-reporting":
    userId: "d05a2d0f-afc4-2a9d-6dd1-6187f038a2e4"
  linter: {}
  "linter-eslint": {}
  "linter-phpcs":
    codeStandardOrConfigFile: "/Users/rrobinson/code/pepperjam/dev/code-standards/PHP\\ CodeSniffer/phpcs.xml"
    disableWhenNoConfigFile: true
    otherLanguages:
      useCSSTools: true
      useJSTools: true
    tabWidth: 4
  "linter-ui-default":
    showPanel: true
  "linter-xmllint": {}
  "merge-conflicts": {}
  minimap: {}
  "one-dark-ui":
    layoutMode: "Compact"
  "php-cs-fixer": {}
  phpcbf: {}
  pigments: {}
  welcome:
    showOnStartup: false
```


## Styles (`styles.less`)
```less
/*
 * Your Stylesheet
 *
 * This stylesheet is loaded when Atom starts up and is reloaded automatically
 * when it is changed and saved.
 *
 * Add your own CSS or Less to fully customize Atom.
 * If you are unfamiliar with Less, you can read more about it here:
 * http://lesscss.org
 */


/*
 * Examples
 * (To see them, uncomment and save)
 */

// style the background color of the tree view
.tree-view {
  // background-color: whitesmoke;
}

// style the background and foreground colors on the atom-text-editor-element itself
atom-text-editor {
  // color: white;
  // background-color: hsl(180, 24%, 12%);
  text-rendering: optimizeLegibility;
}

// To style other content in the text editor's shadow DOM, use the ::shadow expression
atom-text-editor::shadow .cursor {
  // border-color: red;
}

// Disable ligatures within the scope of strings and regular expressions (per FiraCode Mono)
atom-text-editor.editor .syntax--string.syntax--quoted,
atom-text-editor.editor .syntax--string.syntax--regexp {
  -webkit-font-feature-settings: "liga" off, "calt" off;
}
```


## Packages
### Community Packages
- `busy-signal`
- `docblockr`
- `editorconfig`
- `intentions`
- `language-dotfiles`
- `linter`
	- `linter-eslint`
	- `linter-phpcs`
	- `linter-scss-lint`
	- `linter-ui-default`
	- `linter-xmllint`
- `merge-conflicts`
- `minimap`
- `php-cs-fixer`
- `phpcbf`
- `pigments`
- `update-changelogs`
