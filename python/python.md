# Python
- [Guide to Webapps + Databases](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/Python2_Apps.html)
- [Guide to Flask](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/Python3_Flask.html) (including SQLAlchemy, RESTful APIs, Unit Testing, etc.)



## `virtualenv`
- https://dzone.com/articles/building-a-python-virtual-environment


### Initial Setup
```shell
pip install virtualenv
virtualenv myproject
source myproject/venv/bin/activate
(venv) user@machine $ # prompt
(venv) user@machine $ pip install <library file>
(venv) user@machine $ pip freeze > myproject/requirements.txt
```


### Usage
```shell
pip install virtualenv
virtualenv myproject source myproject/venv/bin/activate
(venv) user@machine $ git clone <repo url>
(venv) user@machine $ cd myproject/myrepo
(venv) user@machine $ pip -r requirements.txt
```
