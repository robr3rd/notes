# Markdown
Markdown is a wonderful syntax but it is also somewhat the wild west, as a widely-accepted, centralized standard simply does not exist.  That said, here are the notable ones:

- [Markdown](https://daringfireball.net/projects/markdown/basics) (original)
- [GitHub Flavored Markdown (GFM)](https://github.github.com/gfm/)
- [GitLab Flavored Markdown (GFM)](https://docs.gitlab.com/ee/user/markdown.html)
- [Markdown Extra](https://michelf.ca/projects/php-markdown/extra/)
- [CommonMark spec](https://spec.commonmark.org/) (attempt to unify standards)