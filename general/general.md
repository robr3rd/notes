# General
Please use this category with **great** hesitation...only _truly general_ things should reside here; otherwise, a new category should just be created. This should be things that apply to more than one category.

## Metasyntactic Variables
1. foobar
1. foo
1. bar
1. qux
1. quux
1. quuz
1. corge
1. grault
1. garply
1. waldo
1. fred
1. plugh
1. zyxxy
1. thud
1. wibble (UK)
1. wobble (UK)
1. wubble (UK)
1. flob (UK)
1. blep (AUS)
1. blah (AUS)
1. boop (AUS)

> Metasyntactic variables used in the United States include `foobar`, `foo`, `bar`, `baz`, `qux`, `quux`, `quuz`, `corge`, `grault`, `garply`, `waldo`, `fred`, `plugh`, `xyzzy`, and `thud`. `Wibble`, `wobble`, `wubble`, and `flob` are used in the UK. And there are a reported `blep`, `blah`, and `boop` from Australia.
> [...] `spam`, `ham`, and `eggs` are the principal metasyntactic variables used in the Python programming language. This is a reference to the comedy sketch Spam by Monty Python, the eponym of the language.
> The R programming language often adds `norf` to the list.
>
> - [Metasyntactic Variables, Wikipedia](https://en.wikipedia.org/wiki/Metasyntactic_variable#English)

## Keyboards
### Mechanical
Some good resources include (ignore that this is for a specific brand...good info is good info):

- http://www.daskeyboard.com/blog/mechanical-keyboard-guide/#keyswitches
- http://www.daskeyboard.com/switches
- The "keyboard-switch-types.gif" in this repo


## DNS - CNAME
- https://support.dnsimple.com/articles/differences-between-a-cname-alias-url/
