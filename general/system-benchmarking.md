# System Benchmarking
The `sysbench` CLI tool is an easy and effective way to benchmark various aspects of a system.

For a more well-thought-out explanation of each, check out [this article](https://www.vpsbenchmarks.com/posts/evaluating_cloud_server_performance_with_sysbench); otherwise, enjoy the brief snippets below.



## CPU

### Single Core
```sh
# input
sysbench cpu --cpu-max-prime=20000 --threads=1 --time=60 run  # use 1 core and stop after 60 seconds

# output
CPU speed:
    events per second:   347.98

General statistics:
    total time:                          60.0016s
    total number of events:              20880

Latency (ms):
         min:                                    2.79
         avg:                                    2.87
         max:                                    3.98
         95th percentile:                        3.02
         sum:                                59989.15

Threads fairness:
    events (avg/stddev):           20880.0000/0.00
    execution time (avg/stddev):   59.9891/0.00

```


### Multiple Core
```sh
# input
sysbench cpu --cpu-max-prime=20000 --threads=4 --time=60 run  # use 4 cores and stop after 60 seconds

# multi-core output
CPU speed:
    events per second:  1287.24

General statistics:
    total time:                          60.0026s
    total number of events:              77240

Latency (ms):
         min:                                    2.93
         avg:                                    3.11
         max:                                    5.65
         95th percentile:                        3.36
         sum:                               239931.89

Threads fairness:
    events (avg/stddev):           19310.0000/4.12
    execution time (avg/stddev):   59.9830/0.00
```



## Memory
```sh
# input
sysbench memory --memory-oper=write --memory-block-size=1K --memory-scope=global --memory-total-size=100G --threads=4 --time=30 ru

# output
Total operations: 104857600 (8106933.57 per second)

102400.00 MiB transferred (7916.93 MiB/sec)


General statistics:
    total time:                          12.9326s
    total number of events:              104857600

Latency (ms):
         min:                                    0.00
         avg:                                    0.00
         max:                                   20.00
         95th percentile:                        0.00
         sum:                                26927.76

Threads fairness:
    events (avg/stddev):           26214400.0000/0.00
    execution time (avg/stddev):   6.7319/0.03
```



## Storage I/O
```sh
# input
sysbench fileio --file-total-size=10G prepare

# output
128 files, 81920Kb each, 10240Mb total
# [...]
10737418240 bytes written in 19.90 seconds (514.63 MiB/sec).
```
