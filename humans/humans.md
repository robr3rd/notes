# Humans
Non-tech, human-oriented notes...things about "fuzzy" matters like people and emotions and such.


## Ranking Relationships with Humans
> An excellent write-up by the Insight Engineering Manager of Netflix about ["Quantifying Personal Retention Impact: A Mathematical Thought Exercise"](http://royrapoport.blogspot.com/2015/01/quantifying-personal-retention-impact.html)

I've generally considered a big part of my job to be a positive impact on the retention of my employees and coworkers.  Prone as I am to quantification, I came up with, and in this blog post propose, an exercise for quantifying retention impact.

Firstly, it's more obvious to rate retention impact from the perspective of recruiting attraction -- you get used to the people you work with, and in most cases you're not making active, daily, decisions about whether or not you want to work with them.  Looking at it from the perspective of whether or not you'd pursue a new job with this person clarifies things a bit.  

So for a given person with whom you work, imagine they've left the company, and you're considering leaving the company to go to another company.  You find out they work in that company and your relationship with them would be the same as the relationship you have with them today -- if they're managing you today, they'd be managing you at this new company; if they're a peer today, they'd be a peer in this new company, etc.  What's your response, on a -3 to +3 scale?

- `-3`: No.  Just no.  I don't care if it's the best job in the world, it isn't worth it if I have to renew this relationship with this person; 
- `-2`: Ugh.  Do I have to? Maybe there are some very significant reasons why I want this job and am willing to undertake the pain of this relationship again, but boy howdy it better be a ludicrously great job;
- `-1`: Oh well.  If the job is decent and I was pretty sure I'd take it anyway, I'm not happy to hear this, but I can live with it; 
- `0`: No impact
- `+1`: Oh cool.  I'm somewhat more inclined to take the job because they're part of the package
- `+2`: Wow.  I'm much more inclined to take the job because I'll get to work with them again.  I'm willing to take some pain to do it; 
- `+3`: Almost irrespective of how bad the rest of the offer is, or how terrible the environment, if I get to work with them again I'll do it! 

Think of the process by which a person p comes up with the answer with respects to you as function R, so R(p) has an output between -3 and 3.  

Firstly, it's useful to look at the value of R(p) for the set of people for whom you are most relevant -- direct reports, peers, manager, etc.  You can then expand to other people -- how about people in other teams? The further out, the smaller the amplitude of the likely answers -- it's unlikely that someone who hasn't worked closely with you would consider you a +3 or even a +2, probably, and for the vast majority of people who work in my company, I'm almost definitely a 0 because they don't know who I am, and it's not hugely relevant to them.  

It's noteworthy, however, that numbers for any given relationship can likely have a bigger negative span than a positive one.  For me, for example, I can't imagine a manager to whom I've ever reported who's a +3 -- the best and brightest, the people I'd love to report to again, are +2s.  However, I can definitely think of managers of mine who are -3; similarly, I was talking with my HR business partner and mentioned to her she's a +2 for me -- I really really love our HR group and find both the group and the individuals in it a big factor in why I'm happy where I work -- but I can't imagine an HR person or HR group being so instrumental so as to be a +3.  At the same time, again, I've worked with HR groups who would definitely be a -3.  
