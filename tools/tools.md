# Software & Tools
## Build Tools
- Jenkins (fully-fledged build tool)
- Gulp.js (build tool written on top of NodeJS geared towards web development...NOT Grunt)
- PostCSS

## JavaScript Module Bundling
- Parcel
- Webpack

## Frontend frameworks
- TailwindCSS
- Bootstrap (sometimes)

## Databases
- MySQL Workbench (querying + ERD creation and management)
- JetBrains (DataGrip/PhpStorm/IntelliJ IDEA)

## Virtualization
- VirtualBox (VMs)
- Vagrant (dev interface for VM management)
- Docker (containers)

## Provisioning
- Ansible (NOT Puppet or Chef)

## Diagramming
- Draw.io
- XMind (for brainstorming specifically)
- BPMN (not a "tool" per se but kind of...it's essentially supercharged flowcharts)

## IDEs / Editors
- Atom
- PHPStorm (PHP + JS + CSS + web dev in general)
- Sublime Text
- VSCode
- Nano
- Vim

## Language Managers
- NVM (Node.js Version Manager)
- `rbenv` (*not* RVM)

## Package Managers
- Brew (like an AUR Helper, but for Mac)
- Composer (PHP)
- NPM (Node Package Manager for JS-specific assets that don't also fall under the umbrella covered by "Bower")
- Yarn (NPM but better)
- Pip (Python)

## Miscellaneous
- EditorConfig (simple, bare minimum code standards)

### CLI
- `htop` (system resource monitor)
- `atop` (like `htop` but different focus)
- `tmux`
	- See my `dotfiles` repo for more on this.
	- [Teamocil](https://github.com/remiprev/teamocil) (manage TMux layouts and such via YAML files)
- `zsh`
	- See my `dotfile` repo for more on this.
- [`battery-level`](https://github.com/gillstrom/battery-level)
- [`brightness-cli`](https://github.com/kevva/brightness-cli)
- [`caniuse-cmd`](https://github.com/sgentle/caniuse-cmd)
- [`diff2html-cli`](https://github.com/rtfpessoa/diff2html-cli)
- [`is-up-cli`](https://github.com/sindresorhus/is-up-cli)

### CSS
- CSSComb

### HTTP
- `curl`
	- 90% of manual invocations of this comes down to `curl --request DELETE|GET|HEAD|POST|PUT --url 'https://example.com'`.
- HTTPie

### Javascript
- ESLint (code standards + linting)
- Jest (Unit Testing)
- Jasmine (Unit Tests)

### Load Testing
- `ab` (not perfect...`siege` is better, but `ab` is good for quick, unofficial tests)
	- Examples:
		- `ab -n 500 -c 100 example.localhost` - Send 500 total requests with a concurrency of 100 at a time
		- `ab -n 25 -c 5 example.localhost` - 25 requests in total with concurrency of 5
		- `ab -n 100 -l -c 10 example.localhost` - 1-- total requests, 10 at a time, and `-l` prevents complaints about inconsistent load times (i.e. on dynamic web pages sometimes the page may take longer to generate than other times and this is to be expected).

### PHP
- Faker (convincing, randomly-generated fake data)
- PHPCodeSniffer (PHPCS) (code standards + linting)
- PHPMessDetector (PHPMD) (advanced code standards like cyclomatic dependency testing)
- PHPUnit (Unit Tests)
- Behat (Behavior Tests with Gherkin files)
- Mink (browser drivers for use with Behat)
- Carbon (date library)
- DotEnv (app config management)

### SCSS
- dart-sass (NOT node-sass)
- StyleLint (SCSS linter that succeeds SCSSLint)

### VCS
- Git

### Web Applications (in general)
- Selenium2 (drive real browsers)
- Goutte (PHP-based headless browser)
