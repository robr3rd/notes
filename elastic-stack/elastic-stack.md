# Elastic Stack
Formerly the "ELK Stack", this a set of technologies used together for application/systems montioring.

These tools consist of the following:

- ElasticSearch (data store)
- Logstash (log aggregation, normalization, and enrichment)
- Kibana (dashboard and query UI)
- Beats (log collection and shipment)
    - Filebeat (logs)
    - Metricbeat (host metrics)
    - ...and many more...

I'm going to focus on the things that suck here, since that's what I really care about jotting down for future reference.



## ElasticSearch
### List indices
`GET /_cat/indices?v`

### Removing indices
TL;DR: ElasticSearch has a REST API, so you should just use it--`curl -XDELETE <es_host>:<es_port>/<index>/<type>/<document_id>`--and cast as broad or narro of a net as you desire (e.g., by specifying just `/<index>` or by specific all options).

To just blow up EVERYTHING, including X-Pack credentials and all Kibana data--which stores its settings in ElasticSearch--, `curl -X DELETE 'https://localhost:9200/_all` should also work.



## Filebeat
### Mutliline logs
If you have log entries that span multiple lines (e.g., have `\n` in them somewhere) but belong to the same single "logical" entry, then this is what you want: https://www.elastic.co/guide/en/beats/filebeat/current/multiline-examples.html

This post (https://stackoverflow.com/a/48783433/2564560) is a bit more helpful since it shows an _actual_ implementation, especially the progression from the best answer to https://stackoverflow.com/questions/48745506/logstash-grok-filter-config-for-php-monolog-multi-linestacktrace-logs#comment88438326_48783433 to further split the fields if desired.



## Logstash
### Configtest
Is it broken and you don't know why and there are too many log lines to sift through?  Just do this!!!

`/usr/share/logstash/bin/logstash --path.settings /etc/logstash/ --config.test_and_exit /etc/logstash/conf.d/logstash.conf`

That will do exactly what you think it will--manually invoke Logstash with its default configs but with a flag to JUST TRY IT ONCE.

### Start/Stop warning
Start/stop the service with the usual service management commands, BUT it tends to "start" and just endlessly error without telling you.

Whenever you `systemctl start logstash` this sucker, be sure to ALSO run either `journalctl -u logstash` or `tail -f /var/log/logstash/logstash-plain.log` to REALLY see what's going on.

It almost seems like it wasn't intended to be run as a service, since it has _perfectly fine_ error messaging in and of itself, but it's a problem that the service isn't notified that it will be failing, quitting, then restarted (by systemd or whatever) and do it all over again infinitely.  (BEWARE: these start/fail/die/retry log entries will quickly fill your disk space in fun places like `/var/log/syslog` and `/var/log/daemon.log`.)
