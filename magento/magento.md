# Magento

## History
- http://www.sherodesigns.com/infographic-magento-history/
	- Notably, on 2011-02-08 the first same-day CE/EE release happened--specifically, CE 1.5 and EE 1.10. After this point they were released in sync as well, so that makes this an easy target for "How far back should I support?".

## Marketplace
- Review/Approval Process: http://docs.magento.com/marketplace/user_guide/extensions/review-workflow.html

## Extension Quality Program
This is essentially just Magento's PHP_CodeSniffer rules. These get run against all extension submissions for Technical Review and (in some cases) run again during QA Review, but with more verbose reporting.

- https://github.com/magento/marketplace-eqp
