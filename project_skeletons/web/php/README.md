# PHP Project Skeleton
Some description of the project...

## Requirements
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (tested.version.number+)
- [Vagrant](https://www.vagrantup.com/downloads.html) (tested.version.number+)
	- _(optional)_ [`vagrant-hostsupdater`](https://github.com/cogitatio/vagrant-hostsupdater) to have your `hosts` file handled automatically (`vagrant plugin install vagrant-hostsupdater`)

## Setup
1. `cd provisioning`
1. `ansible-galaxy install -r requirements/[environment].yml`
1. `vagrant up`

## Developing
### Servers & Services Overview
- Servers:
	- Hostname: `foobar`
	- Website: `foobar.localhost`
		- Document Root: `/srv/http/`
	- Database: `N/A`
		- User: `N/A`
		- Password: `N/A`

### Database - Connecting to MySQL
To connect to the MySQL database using a MySQL client (e.g. PhpStorm, MySQL Workbench, Sequel Pro) you must remember to do so via SSH Tunnel.

- SSH Tunnel
	- Host: `foobar.localhost`
	- Port: `22`
	- User: `vagrant`
	- Pass: `vagrant`
- General
	- Host: 127.0.0.1
	- Port: 3306
	- User: [database name]
	- Pass: [database name]
	- Database: [database name]
