# Cron

## Reference Guide
```
* * * * *	    Every minute
0 * * * *       Every hour (on the hour)
1 * * * *	    The first minute of every hour
*/15 * * * *	Every 15 minutes, starting on the hour
0 3 * * *       Every day at 03:00
30 2 * * *      Every day at 02:30
0 0 * * Mon,Thu 00:00 every Monday and Thursday
45 9 4 * *	    Fourth day of the month at 09:45
0 18-22 * * *   Every hour from 6pm to 10pm
```

