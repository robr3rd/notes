# Docker - Intro
There are two primary components of Docker to understand: [Docker Engine vs Docker Machine](https://docs.docker.com/machine/overview/#why-should-i-use-it)



## Docker Engine
This is the "backend" that actually spins up, manipulates, and tears down Docker containers.



## Docker Machine
> Tip: For shell completion (Zsh _and_ Bash): https://docs.docker.com/machine/completion/#installing-command-completion

This is essentially "a remotely-installed Docker Engine" for when your local system cannot run Docker Engine, itself.

Basically, if you're on Linux then you should just install Docker Engine, but if you are _not_ on Linux, then you'll need a Linux-based Docker Machine that you can leverage (remotely) to start/stop containers due to your local OS not being able to do so, itself.


### Accessing your Machine
```bash
docker-machine ls

docker-machine ssh <one of the machines in the list>
```


### Drivers
> https://docs.docker.com/machine/drivers/

There are many Drivers for Docker Machine, including:

- Virtual Machine software: VirtualBox, VMware, Hyper-V
- Cloud Providers: AWS, Digital Ocean, Google Compute Engine, Microsoft Azure, OpenStack, Rackspace Cloud
- Generic: "Create machines using an existing VM/Host with SSH." (https://docs.docker.com/machine/drivers/generic/)


### Usage
#### Create
##### Rackspace Cloud
```bash
# Create a new VM (Debian 9 (Stretch) (PVHVM)) in the form of a 4GB General Purpose v1 server.
# Tip for the 'rackspace-image-id': go to the Rackspace Cloud Control Panel --> Create a new Server --> 'Inspect Element' with your browser to get the hash from the HTML.
# Tip for the 'rackspace-flavor-id': '[class][version]-[ram-qty]'. So 'compute1-15' would be '15GB Compute v1' and 'general1-4' would be '4GB General Purpose v1'.
docker-machine create --driver 'rackspace' --rackspace-username 'YOUR-USERNAME' --rackspace-api-key 'YOUR-API-KEY' --rackspace-region 'IAD' --rackspace-image-id 'c6cc1ab6-f354-4c6b-8630-b10ae3cad204' --rackspace-flavor-id 'general1-4' NEW-SERVER-HOSTNAME
```

#### Use (switch local shell's context to be "as if" it were for the remote Docker Machine
If you've used and understand how Python's `virtualenv` works, you'll feel right at home here.

```bash
eval $(docker-machine env NEW-SERVER-HOSTNAME)
```

##### Test
```bash
# local machine (tty1)
docker container list # empty

# local machine (tty1)
docker run -it alpine

# local machine (tty2)
docker container list # alpine
```

```bash
# shell with docker-machine env imported (tty1)
docker container list # empty!

# docker-machine shell (tty1)
docker run -it alpine

# docker-machine shell (tty2)
docker container list
```

#### Stop using
```bash
eval $(docker-machine env -u)
```
