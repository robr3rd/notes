#! /usr/bin/env bash
#
# SSH Key Forwarding for Docker **containers**
#
# Context: Docker doesn't support SSH Key Forwarding in Containers.  The closest
# thing that it offers is leveraging `DOCKER_BUILDKIT` while building images.
#
# This tool:
# - retrieves the filepaths of all SSH Keys currently in your keychain
# - mounts each of them as a Docker Volume in a basic Debian container
# - installs `openssh-client` (for `ssh-add`)
# - starts an SSH keychain server
# - adds each of your hosts's keys to the guest's keychain
# - prints the list of the guest's keys
#
# Requirements:
# - `ssh-add -L` must work on your host machine
# - Docker must be installed
# - The target container must have `apt` installed
#

arg_volumes="$(echo $(ssh-add -L | awk '{ printf "%s %s:%s", "--volume", $3, $3 }' | sed -E 's@:[^\.]+(\.ssh/[^ ]+)@:/root/\1@g'))"
cmd_ssh_add="$(echo $arg_volumes | sed -E 's/(-v|--volume) [^:]+:([^ ]+)/ssh-add \2 \&\&/g')"

echo "arg_volumes: $arg_volumes"
echo "cmd_ssh_add: $cmd_ssh_add"

docker run \
	--rm \
	-it \
	$(echo $arg_volumes) \
debian \
bash -c 'apt update && \
	apt install -y ssh-client && \
	eval $(ssh-agent -s) && \
	'$(echo $cmd_ssh_add)' && \
	echo -e "\n\n\nNow to reveal the known keys!\n\n" && \
	ssh-add -L'
