# Docker - Commands

## Container Statuses (simple)
```bash
docker ps -a --format "table {{.Names}}\t{{.ID}}\t{{.RunningFor}}\t{{.Image}}\t{{.Ports}}"
```

## Update all images
```bash
docker images | grep -v REPOSITORY | awk '{print $1}' | xargs -L1 docker pull
```

## Update all images (within current tag)
```bash
docker images | grep -v REPOSITORY | awk '{printf $1; printf ":"; print $2}' | xargs -L1 docker pull
```
