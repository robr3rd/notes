# Docker - Docker Machine

## Create new Docker Machine remotely in Rackspace Cloud
### Syntax/Template
```bash
docker-machine create --driver 'rackspace' --rackspace-username "$USERNAME" --rackspace-api-key "$API_KEY" --rackspace-region "$REGION" --rackspace-image-id "$IMAGE_ID" --rackspace-flavor-id "$FLAVOR_ID" <hostname>
```

### Example
```bash
docker-machine create --driver 'rackspace' --rackspace-username "robr3rd" --rackspace-api-key 'd4a760a2e43044abae5357c3b703bb84' --rackspace-region 'IAD' --rackspace-image-id '2d970ae8-c358-4284-a43f-47cfd5968b70' --rackspace-flavor-id 'general1-4' <hostname>
```

### Where do I find that information?
- Username: It's what you log in with
- API Key: Account Settings
- Region: "Create Server" page has a dropdown of regions
- Image ID: (unnecessarily complex) https://developer.rackspace.com/docs/cloud-images/v2/getting-started/use-images/#listing-images (look for `!onmetal` or PVHVM for non-Metal servers)
- Flavor ID: https://developer.rackspace.com/docs/cloud-servers/v2/general-api-info/flavors/#virtual-cloud-server-flavors