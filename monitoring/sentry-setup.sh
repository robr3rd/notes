#! /usr/bin/env bash

# From: https://hub.docker.com/_/sentry/

# Make a home for this and go there
mkdir sentry-test && cd sentry-test

# Service setup
docker run -d --name sentry-redis redis
docker run -d --name sentry-postgres -e POSTGRES_PASSWORD=secret -e POSTGRES_USER=sentry postgres

# Generate a secret key (quick spin up, gen, then removal)
SENTRY_SECRET_KEY="$(docker run --rm sentry config generate-secret-key)" && echo $SENTRY_SECRET_KEY

# Sentry server setup (migrations, etc.)
docker run -it --rm -e SENTRY_SECRET_KEY="'$SENTRY_SECRET_KEY'" --link sentry-postgres:postgres --link sentry-redis:redis sentry upgrade

# Spin up actual Sentry server (http://localhost:9000)
docker run -d --name my-sentry -p 9000:9000 -e SENTRY_SECRET_KEY="$SENTRY_SECRET_KEY" --link sentry-redis:redis --link sentry-postgres:postgres sentry

# Cron & Worker setup
docker run -d --name sentry-cron -e SENTRY_SECRET_KEY="$SENTRY_SECRET_KEY" --link sentry-postgres:postgres --link sentry-redis:redis sentry run cron
docker run -d --name sentry-worker-1 -e SENTRY_SECRET_KEY="$SENTRY_SECRET_KEY" --link sentry-postgres:postgres --link sentry-redis:redis sentry run worker

echo 'The UI may be found at http://<host>:9000.'
