# Vagrant - Ansible Galaxy - Private Repos
For a project that I was working on that leveraged Vagrant-based local/dev provisioning, I wanted to use Ansible Galaxy to include some custom roles from a few private repos.

Alright, so solution/documentation time (since it was a little weirder than I expected).

Considering that in our case Vagrant is just installing Ansible on the box automatically and that the Galaxy stuff has to happen post-Ansible install but pre-provision execution, logically this has to be handled by Vagrant.

Fortunately, Vagrant does support the installation of Galaxy roles OOTB.  Let’s see what that looks like:

```ruby
Vagrant.require_version '>= 2.0.0'

Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"

  config.ssh.forward_agent = true

  # app server
  config.vm.define "my-app" do |app|
  
    app.vm.hostname = "my-app"
    app.vm.synced_folder ".vagrant", "/vagrant/.vagrant", mount_options: ["dmode=775,fmode=600"]
    
    app.vm.provider :virtualbox do |vb|
      vb.name = "my-app" # Name that appears in VirtualBox
      vb.memory = "1024" # The amount of memory for the VM
      vb.cpus = "1" # The VM's number of CPU cores
    end
  
    ## Provisioning
    app.vm.provision :ansible_local do |ansible|
      ### "Vagrant, install THIS Ansible"
      ansible.install_mode = :pip
      ansible.version = "2.8.2"

      ### Playbook arguments
      ansible.playbook = "local.yml"
      ansible.inventory_path = "inventories/local"
      ansible.limit = "all"
      # ansible.verbose = "v"

      ### Install Dependencies
      ansible.galaxy_role_file = 'requirements.yml'
      ansible.galaxy_roles_path = 'roles'
      
      # override command since OOTB includes `--force` which ALWAYS re-installs roles even if already installed
      ansible.galaxy_command = 'ansible-galaxy install --role-file=%{role_file} --roles-path=%{roles_path}'
    end
  end
end
```

Awesome!  That’s a fairly reasonable syntax and it’s even mostly self-documenting and such!

…but when we run it…

```yaml
[WARNING]:
  - username.private-repo was NOT installed successfully:
    - command /usr/bin/git clone git@vcs-host.username/private-repo.git 
    
username.private-repo failed in directory /root/.ansible/tmp/ansible-local-17200rq7tjs4i/tmpsji2fo_v (rc=128)

ERROR! - you can use --ignore-errors to skip failed roles and finish processing the list.
```

Well, darn!  Ansible Galaxy is unable to install our **private roles** because it lacks the SSH Private Key to authenticate!

We could, of course, figure out a way to try and mount that, but considering that everyone manages their keys differently that just sounds like a nightmare that will break _somebody’s_ workflow.  After all, that is the **whole reason** we have that nice little `config.ssh.forward_agent = true` setting in the first place--so people can just use it as a “jump box” by essentially taking their unlocked keys with them any time they SSH in!

Okay, okay…so…what now?

Well, at this point we can't really let Vagrant handle the Galaxy installation since that needs to be handled via `vagrant ssh` at this point, e.g., `vagrant ssh app -c 'ansible-galaxy install -r requirements.yml'`.

But wait!

We can't just run `vagrant up` and **_then_** run Ansible Galaxy!!!  The whole _point_ of running Galaxy is to _install things that Ansible needs to provision the box_!

Okay, fine…

Looks like we’ll just have to take care of all of this “Ansible” business ourselves so that we can inject whatever we need before/after each step by just seizing full control of the situation.

Let’s see what that looks like…

**Commands**
```bash
vagrant up
vagrant ssh my-app -c '/vagrant/provision-local.sh'
```

**Vagrantfile**
```ruby
Vagrant.require_version '>= 2.0.0'

Vagrant.configure("2") do |config|
  config.vm.box = "debian/stretch64"

  config.ssh.forward_agent = true

  # app server
  config.vm.define "my-app" do |app|
end
```

**provision-local.sh**
```bash
#! /usr/bin/env bash
#
# Install necessary tools on Debian VM for provisioning
#
# This script was created to essentially replace Vagrant's `config.vm.provision :ansible_local`...
# ...because it does not support running Ansible Galaxy with SSH key forwarding; therefore, Galaxy...
# ...will fail when trying to install roles from any PRIVATE repos since the SSH key auth will fail.
#
# Syntax: ./provision-local.sh [Pip-compatible ansible_version]
#
# Usage:
# - Default: ./provision-local.sh
# - Custom:  ./provision-local.sh "~=2.6"
#

ansible_version="${1:-~=2.8.2}"


echo "Trusting RSA key fingerprint of VCS Host, i.e., Bitbucket..."
if ! ssh-keyscan -t rsa bitbucket.org >> /home/vagrant/.ssh/known_hosts; then
	echo "Unable to retrieve RSA key fingerprint of VCS host."
	exit 1
fi


echo "Prepare to install Ansible on guest..."
if ! sudo apt-get install -y git python3-pip; then
	echo "Preparations for Ansible installation on guest failed."
	exit 1
fi


echo "Installing Ansible on guest..."
if ! sudo pip3 install "ansible${ansible_version}"; then
	echo "Ansible installation on guest failed."
	exit 1
fi


echo "Installing provisioning dependencies..."
if ! (cd /vagrant && ansible-galaxy install -r requirements.yml); then
	echo "Provisioning dependencies failed to install."
	exit 1
fi


echo "Provisioning guest VM..."
if ! (cd /vagrant && ansible-playbook -i inventories/dev dev.yml); then
	echo "Guest provisioning failed."
	exit 1
else
	echo "Provisioned guest successfully."
fi
```