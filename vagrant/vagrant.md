# Vagrant

## How `vagrant ssh` works
`vagrant ssh` internally is running the following command:

> `ssh -p 2222 -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o LogLevel=ERROR -o IdentitiesOnly=yes -i ~/.vagrant.d/insecure_private_key vagrant@127.0.0.1`
> - http://stackoverflow.com/questions/16244601/vagrant-reverse-port-forwarding

With this knowledge (and some knowledge of the `ssh` command / `man ssh`), you can "roll your own" custom solution to meet a variety of needs.

If said level of customization is desired, then this should be done with the `--` (double-dash) argument delimiter so that it is explicitly stated that `vagrant ssh`'s arguments are done. As an example: `vagrant ssh -- [your custom SSH options]`.



## Port forwarding between host and guest
Let's say your local machine has an active instance of MySQL on port `3306` (i.e. the default), but you want to connect to the _Vagrant box's_ MySQL instance while still retaining the comfort and convenience of your own toolkit running directly on your own metal... What do you do?

You configure port forwarding!

By running `vagrant ssh -- -R 3307:vagrant-project:3306` on your host, all requests that your host makes to `vagrant-project:3307` will hit the Vagrant box and then be internally re-routed back to it again on port `3306` so that it's "as if" nothing at all is different.

The syntax of this command is as follows: `vagrant ssh -- -R [fake port]:[host name]:[desitnation port]`.



## Vagrantfile Syntax Highlighting for PhpStorm
1. Clone this repo: https://github.com/textmate/ruby.tmbundle
2. In PhpStorm's Settings, open the `TextMate Bundles` category.
3. Add (`+`) and select the directory of the cloned repository.
4. At the bottom of the `TextMate Bundles` settings, there is a table with two columns: `IDE Color Scheme | TextMate Color Scheme`. Make sure to select the appropriate match for your theme; otherwise (especially if you use a dark theme), you may end up with your nice, dark `IDE Color Scheme` being ruined by a stark white/default-colored `Color Scheme` in your code editor pane.



## DHCP IP Address Reporting
Since use of `config.vm.network :private_network, type: :dhcp` causes the guest machine's IP address to be unknown (unless you SSH in and run `ifconfig`, of course, but that's realy inconvenient), you can instead just add this snippet to your Vagrantfile after defining all of your machines (or to specific ones if so desired, such as "just the one or two using DHCP" dependnig on your environment is defined.):

```ruby
# Print the assigned IP address to stdout (intended for use with `vm.network [...], type: :dhcp` since it's not known)
# Note: Requires the 'vagrant-triggers' plugin
# Source: (http://stackoverflow.com/a/28835516/2564560)
config.trigger.after :up, :stdout => false, :stderr => false do
get_ip_address = %Q(vagrant ssh #{@machine.name} -c 'ifconfig | grep -oP "inet addr:\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}" | grep -oP "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}" | tail -n 2 | head -n 1')
@logger.debug "Running `#{get_ip_address}`"
output = `#{get_ip_address}`
@logger.debug "Output received:\n----\n#{output}\n----"
puts "==> #{@machine.name}: Available on DHCP IP address #{output.strip}"
@logger.debug "Finished running :after trigger"
end
```
