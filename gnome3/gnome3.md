# GNOME 3
## Autostart
```
# ~/.config/autostart/foobar-baz.desktop

[Desktop Entry]
Name=MyScript
GenericName=A descriptive name
Comment=Some description about your script
Exec=/path/to/my/script.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true
```

- [StackOverflow](http://stackoverflow.com/questions/8247706/start-script-when-gnome-starts-up)
- [Autostart specs](https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html)


## Desktop Entry
- [Desktop Entry specs](https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html)
- See example sibling file to this one named `google-services-launcher.desktop` for an example of one that I used for years (but no longer due to lack of necessity).
