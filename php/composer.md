# PHP - Composer

## Automatically Authenticating to Private Repositories
> - Short version: Use `~/.composer/auth.json` (https://getcomposer.org/doc/06-config.md#bitbucket-oauth)
> - Homework version: Knowledge of OAuth2 is helpful but certianly not required for this: https://tools.ietf.org/html/rfc6749#page-9

For `composer install` to work when one or more of its target repos are private, authentication tokens must be configured.

The exact process to do so varies by vendor and may change over time so any vendor-specific details won't be covered much here, but just know that something resembling the following should be the end result:

```json
{
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": "ABCde1GhiJKlm2opQR",
            "consumer-secret": "aBCde12Fg3hijKlMnopQrS4TUVw5YZAB",
        }
    },
    "github-oauth": {
        "github.com": "a12b34567c89de01f23ghi456j7klm891n234567"
    }
}
```

...and as it gets used, Composer may add additional information to the configuration, such as the OAuth2 token and its expiration timestamp:

```json
{
    "bitbucket-oauth": {
        "bitbucket.org": {
            "consumer-key": "ABCde1GhiJKlm2opQR",
            "consumer-secret": "aBCde12Fg3hijKlMnopQrS4TUVw5YZAB",
            "access-token": "1AB2CD3efgHijkLmnOpQrST4u5vwX_6yzabC-dEfgHIJKLMNopq_78R0StU1vWXyZ-ABCD1ef2G3ijK-LmN=",
            "access-token-expiration": 1553144573
        }
    },
    "github-oauth": {
        "github.com": "a12b34567c89de01f23ghi456j7klm891n234567"
    }
}
```

### When do I need this?
You will need to do this when you receive a message similar to the following:
```
Could not fetch https://bitbucket.org/socialauth/login/atlassianid/?next=%2Frobr3rd%2Fexample-repo-name%2Fget%<commit_hash>.zip, please create a bitbucket OAuth token to access private repos
Follow the instructions on https://confluence.atlassian.com/bitbucket/oauth-on-bitbucket-cloud-238027431.html
to create a consumer. It will be stored in "<$HOME>/.composer/auth.json" for future use by Composer.
Ensure you enter a "Callback URL" (http://example.com is fine) or it will not be possible to create an Access Token (this callback url will not be used by composer)
Consumer Key (hidden):
```