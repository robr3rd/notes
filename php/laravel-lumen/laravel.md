# Laravel

## Packages
- i18n
	- https://github.com/themsaid/laravel-langman
		- Manage language files via `artisan`
	- https://github.com/themsaid/laravel-langman-gui
		- Manage language files via a web GUI (same creator as the non-`-gui` version of this package)
	- https://github.com/barryvdh/laravel-translation-manager
		- Manage languages via a web interface
		- Note: this moves the languages to the DB rather than the filesystem (although there is a simple importer tool), so be sure to consider the implications of that.
- IVR (interactive voice response)
	- https://laravel-news.com/laravel-hotline-ivr
