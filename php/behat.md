# PHP - Behat
## Links
- Intro/101: https://cucumber.ghost.io/blog/intro-to-bdd-and-tdd/
- Framework integration starter: https://matthewdaly.co.uk/blog/2017/02/18/integrating-behat-with-laravel/
- AMAZING explanation of TableNode input: https://blog.whiteoctober.co.uk/2012/09/12/behat-tablenodes-the-missing-manual/
- Multiple contexts: http://behat.org/en/latest/user_guide/context.html
- The Mink README: https://github.com/Behat/MinkExtension/blob/master/doc/index.rst


## Practical Sample behat.yml config file
```yaml
default:
    extensions:
        Behat\MinkExtension:
            base_url: "http://example.com"
            browser_name: chrome
            sessions:
                session1:
                    selenium2: ~
                session2:
                    goutte: ~
    suites:
        browser:
            mink_session: session1
            contexts:
                - BrowserContext
        headless:
            mink_session: session2
            contexts:
                - HeadlessBrowserContext
            filters:
                tags: "~@javascript" # Goutte doesn't support JS, so skip it
```