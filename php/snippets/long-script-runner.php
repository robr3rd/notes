<?php
/**
 * This script is intended to be used as a "middleman" for when user requests (via web browser) a long-running script
 * This is because most browsers will eventually give up (or crash) when encoountered with a page that takes "too long" to respond
 *
 * Usage
 * Call this script and pass in the ultimate destination path (like a same-origin JSONP)
 *
 * Example
 * https://example.com?path=/path/to/script
 */

$curl_handler = curl_init();
$url = 'http://' . $_SERVER['HTTP_HOST'] . $_GET['path'];
curl_setopt($curl_handler, CURLOPT_URL, $url);
curl_exec($curl_handler);
curl_close($curl_handler);
