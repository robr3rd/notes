<?php
/**
 * Normalize P.O. Box addresses because the system this was written for was in a state of absolute chaos and...
 * ...validations were rejecting non-changes to existing data
 */
$Database = new Database;

$Database->sqlQuery('SELECT id AS entity_id, address FROM entities
					WHERE address LIKE BINARY "po box%"
					   OR address LIKE BINARY "P O Box%"
					   OR address LIKE BINARY "P O BOX%"
					   OR address LIKE BINARY "PO box%"
					   OR address LIKE BINARY "Po Box%"
					   OR address LIKE BINARY "P. O. BOX%"
					   OR address LIKE BINARY "P O box%"
					   OR address RLIKE "^P\.O\. [^Box].*"
					   OR address LIKE BINARY "P.O.Box%"
					   OR address LIKE BINARY "P.O.BOX%"
					   OR address LIKE BINARY "POB %"
					   OR address LIKE BINARY "P.O BOX%"
					   OR address LIKE BINARY "PO BOx%"');
$issues = $Database->sth->fetchAll(PDO::FETCH_ASSOC);

$Database->beginTransaction();

try {
	$counter = 0;

	foreach ($issues as $issue) {
		$new_value = preg_replace('/^POB( .+)$/', 'P.O. Box\1', $issue['address']);
		$new_value = preg_replace('/^(\s)?(P|p)(\.)?( )?(O|o)(\.)?( )?(box|bOx|boX|BOx|bOX|Box|BOX)?( .+)$/', 'P.O. Box\9', $new_value); // Correct the word "Box", along with camelCase, ReverseCamelcase, and all-lowercase "PO"
		$Database->sqlQuery('UPDATE entities SET address = :new_value WHERE id = :entity_id', array(':entity_id'=>$issue['entity_id'], ':new_value'=>$new_value));

		$counter += 1;
	}

	$Database->commit();
	echo 'Successfully updated ' . $counter . ' addresses!';
} catch (Exception $error) {
	$Database->rollback();
	echo $error->getMessage();
}
