<?php
/**
 * This file takes the last name of the entity field and finds any suffixes putting them in the CORRECT column (`suffix`)
 * This is necessary because previous schemas did not have a Suffix column, so suffixes were thrown in with last names.
 *
 * P.S.: This is very similar to the "split-entity-fullname" file. This is basically a dumbed-down version of that.
 */
$Database = new Database;

$Database->sqlQuery('SELECT id, last FROM entities');
$entities = $Database->sth->fetchAll(PDO::FETCH_ASSOC);


// All are lowercase so we can just strotolower() it and not worry about matching all cases
// Some suffixes are in parantheses "()". For those, we need to strip them out, so for that if it is NOT in this array, we try stripping out () and check again.
$suffix_replacements = array(
	'1'		=>	'I',
	'i'		=>	'I',
	'#1'	=>	'I',
	'(1)'	=>	'I',
	'(1st)'	=>	'I',
	'2'		=>	'II',
	'ii'	=>	'II',
	'#2'	=>	'II',
	'2nd'	=>	'II',
	'(2)'	=>	'II',
	'(2nd)'	=>	'II',
	'3'		=>	'III',
	'iii'	=>	'III',
	'#3'	=>	'III',
	'3rd'	=>	'III',
	'(3)'	=>	'III',
	'(3rd)'	=>	'III',
	'4'		=>	'IV',
	'iv'	=>	'IV',
	'#4'	=>	'IV',
	'4th'	=>	'IV',
	'(4)'	=>	'IV',
	'(4th)'	=>	'IV',
	'5'		=>	'V',
	'v'		=>	'V',
	'#5'	=>	'V',
	'5th'	=>	'V',
	'(5)'	=>	'V',
	'(5th)'	=>	'V',
	'6'		=>	'VI',
	'vi'	=>	'VI',
	'#6'	=>	'VI',
	'6th'	=>	'VI',
	'(6)'	=>	'VI',
	'(6th)'	=>	'VI',
	'7'		=>	'VII',
	'vii'	=>	'VII',
	'#7'	=>	'VII',
	'7th'	=>	'VII',
	'(7)'	=>	'VII',
	'(7th)'	=>	'VII',
	'8'		=>	'VIII',
	'viii'	=>	'VIII',
	'#8'	=>	'VIII',
	'8th'	=>	'VIII',
	'(8)'	=>	'VIII',
	'(8th)'	=>	'VIII',
	'9'		=>	'IX',
	'ix'	=>	'IX',
	'#9'	=>	'IX',
	'9th'	=>	'IX',
	'(9)'	=>	'IX',
	'(9th)'	=>	'IX',
	'10'	=>	'X',
	'x'		=>	'X',
	'#10'	=>	'X',
	'10th'	=>	'X',
	'(10)'	=>	'X',
	'(10th)'=>	'X',
	'sr'	=>	'Sr.',
	'sr.'	=>	'Sr.',
	'jr'	=>	'Jr.',
	'jr.'	=>	'Jr.',
);


$headers = array('Entity ID', 'Last Name', 'Reason');

$file = fopen('/tmp/manual-entity-last-names.csv', 'w');
fputcsv($file, $headers);

try {
	// $Database->beginTransaction();
	foreach ($entities as $key => $entity) {
		if (stristr($entity['last'], ' ')) { // Split the name into separate fields, since it's currently stored as one
			$exploded_entity_last_name = explode(' ', $entity['last']);

			// Last [space] Suffix
			if (count($exploded_entity_last_name) === 2) {
				// If the second part is a suffix, we're good...
				if (array_key_exists(strtolower($exploded_entity_last_name[1]), $suffix_replacements)) {
					$values_to_bind = array(
						':last'		=>	$exploded_entity_last_name[0],
						':suffix'	=>	$suffix_replacements[strtolower($exploded_entity_last_name[1])],
						':id'		=>	$entity['id']
					);
					$Database->sqlQuery('UPDATE entities SET `last`=:last, `suffix`=:suffix WHERE id = :id', $values_to_bind);

				// If it's not, then we probably have somebody like the elseif(...>2) below this
				} else {
					fputcsv($file, array($entity['id'], $entity['last']));
				}

			// Last [space] ... [space]
			} elseif (count($exploded_entity_last_name > 2)) {
				fputcsv($file, array($entity['id'], $entity['last'], 'More than 2 spaces'));
			}

		// If no space (implied because of the "if" before this), but one of the suffixes appear somewhere in the name (Example: 'RobinsonIII'), take note
		} else {
			foreach ($suffix_replacements as $key => $value) {
				// Exclude letters that occur naturally in last names
				if ($key !== 'i' && $key !== 'v' && $key !== 'x') {
					if (stristr(strtolower($entity['last']), $key)) {
						fputcsv($file, array($entity['id'], $entity['last'], 'Something suffix-like ("' . strtoupper($key) . '") appears somewhere in the Last Name, but there are no spaces. Unsure of how to handle.'));
					}
				}
			}
		}
	}

	fclose($file); // Done with the CSV

	// $Database->commit();
} catch(Exception $error) {
	// $Database->rollback();
	echo 'An error was encountered. Here is the message: ' . $error->getMessage();
}
