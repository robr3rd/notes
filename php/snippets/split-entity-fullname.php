<?php
/**
 * This file takes the "catch-all" entity `name` field and splits it out into separate fields for "first, middle, last, and suffix"
 */
$Database = new Database;

$Database->sqlQuery('SELECT id AS entity_id, name FROM entities');
$entities = $Database->sth->fetchAll(PDO::FETCH_ASSOC);


// All are lowercase so we can just strotolower() it and not worry about matching all cases
// Some suffixes are in parantheses "()". For those, we need to strip them out, so for that if it is NOT in this array, we try stripping out () and check again.
$suffix_replacements = array(
	'1'		=>	'I',
	'i'		=>	'I',
	'#1'	=>	'I',
	'(1)'	=>	'I',
	'(1st)'	=>	'I',
	'2'		=>	'II',
	'ii'	=>	'II',
	'#2'	=>	'II',
	'2nd'	=>	'II',
	'(2)'	=>	'II',
	'(2nd)'	=>	'II',
	'3'		=>	'III',
	'iii'	=>	'III',
	'#3'	=>	'III',
	'3rd'	=>	'III',
	'(3)'	=>	'III',
	'(3rd)'	=>	'III',
	'4'		=>	'IV',
	'iv'	=>	'IV',
	'#4'	=>	'IV',
	'4th'	=>	'IV',
	'(4)'	=>	'IV',
	'(4th)'	=>	'IV',
	'5'		=>	'V',
	'v'		=>	'V',
	'#5'	=>	'V',
	'5th'	=>	'V',
	'(5)'	=>	'V',
	'(5th)'	=>	'V',
	'6'		=>	'VI',
	'vi'	=>	'VI',
	'#6'	=>	'VI',
	'6th'	=>	'VI',
	'(6)'	=>	'VI',
	'(6th)'	=>	'VI',
	'7'		=>	'VII',
	'vii'	=>	'VII',
	'#7'	=>	'VII',
	'7th'	=>	'VII',
	'(7)'	=>	'VII',
	'(7th)'	=>	'VII',
	'8'		=>	'VIII',
	'viii'	=>	'VIII',
	'#8'	=>	'VIII',
	'8th'	=>	'VIII',
	'(8)'	=>	'VIII',
	'(8th)'	=>	'VIII',
	'9'		=>	'IX',
	'ix'	=>	'IX',
	'#9'	=>	'IX',
	'9th'	=>	'IX',
	'(9)'	=>	'IX',
	'(9th)'	=>	'IX',
	'10'	=>	'X',
	'x'		=>	'X',
	'#10'	=>	'X',
	'10th'	=>	'X',
	'(10)'	=>	'X',
	'(10th)'=>	'X',
	'sr'	=>	'Sr.',
	'sr.'	=>	'Sr.',
	'jr'	=>	'Jr.',
	'jr.'	=>	'Jr.',
);


$headers = array('Entity ID', 'Entity Name');

$file = fopen('/tmp/manual-entity-names.csv', 'w');
fputcsv($file, $headers);

try {
	// $Database->beginTransaction();
	foreach ($entities as $key => $entity) {
		if (stristr($entity['name'], ' ')) { // Split the name into separate fields, since it's currently stored as one
			$exploded_entity_name = explode(' ', $entity['name']);

			// First [space] Last
			if (count($exploded_entity_name) === 2) {
				$values_to_bind = array(
					':first'	=>	$exploded_entity_name[0],
					':last'		=>	$exploded_entity_name[1],
					':id'		=>	$entity['id']
				);
				$Database->sqlQuery('UPDATE entities SET `first`=:first, `last`=:last WHERE id = :id', $values_to_bind);

			// First [space] Middle [space] Last -----OR----- First [space] Last [space] Suffix
			} elseif (count($exploded_entity_name === 3)) {
				// If the last part is a suffix instead of a last name, assume First|Last|Suffix
				if (array_key_exists(strtolower($exploded_entity_name[2]), $suffix_replacements)) {
					$values_to_bind = array(
						':first'	=>	$exploded_entity_name[0],
						':last'		=>	$exploded_entity_name[1],
						':suffix'	=>	$suffix_replacements[strtolower($exploded_entity_name[2])],
						':id'		=>	$entity['id']
					);
					$Database->sqlQuery('UPDATE entities SET `first`=:first, `last`=:last, `suffix`=:suffix WHERE id = :id', $values_to_bind);
				// If the second part is a suffix (e.g. Robinson III Robert because some people actually did that), assume Last|
				} elseif (array_key_exists(strtolower($exploded_entity_name[1]), $suffix_replacements)) {
					$values_to_bind = array(
						':first'	=>	$exploded_entity_name[0],
						':last'		=>	$exploded_entity_name[1],
						':suffix'	=>	$suffix_replacements[strtolower($exploded_entity_name[1])],
						':id'		=>	$entity['id']
					);
					$Database->sqlQuery('UPDATE entities SET `first`=:first, `last`=:last, `suffix`=:suffix WHERE id = :id', $values_to_bind);
				// If the last part is NOT a suffix, we'll assume First|Middle|Last
				} else {
					$values_to_bind = array(
						':first'	=>	$exploded_entity_name[0],
						':middle'	=>	$exploded_entity_name[1],
						':last'		=>	$exploded_entity_name[2],
						':id'		=>	$entity['id']
					);
					$Database->sqlQuery('UPDATE entities SET `first`=:first, `middle`=:middle, `last`=:last WHERE id = :id', $values_to_bind);
				}

			// First [space] Middle [space] Last [space] Suffix
			} elseif (count($exploded_entity_name === 4)) {
				// If $exploded_entity_name[3] is a suffix, we're good...
				if (array_key_exists(strtolower($exploded_entity_name[3]), $suffix_replacements)) {
					$values_to_bind = array(
						':first'	=>	$exploded_entity_name[0],
						':middle'	=>	$exploded_entity_name[1],
						':last'		=>	$exploded_entity_name[2],
						':suffix'	=>	$suffix_replacements[strtolower($exploded_entity_name[3])],
						':id'		=>	$entity['id']
					);
					$Database->sqlQuery('UPDATE entities SET `first`=:first, `last`=:last, `suffix`=:suffix WHERE id = :id', $values_to_bind);
				// If it's not, then we probably have somebody like the elseif(...>4) below this
				} else {
					fputcsv($file, array($entity['entity_id'], $entity['name']));
				}

			// Somebody needs to manually evaluate "Haba Jihab Hurag Wantika Sudi Jurnsgot"
			} elseif (count($exploded_entity_name > 4)) {
				fputcsv($file, array($entity['entity_id'], $entity['name']));
			}

		// There are not any spaces -- needs to be manually resolved
		} else {
			fputcsv($file, array($entity['entity_id'], $entity['name']));
		}
	}

	fclose($file); // Done with the CSV

	// $Database->commit();
} catch(Exception $error) {
	// $Database->rollback();
	echo 'An error was encountered. Here is the message: ' . $error->getMessage();
}
