# PDO (PHP Data Objects)
## Retrieving data (special formats)
Fetching one record at a time (`$statementHandler->fetch()` in a `while` loop) along with fetching an entire result set (`$sth->fetchAll()`), but there are some surprisingly-common useful techniques _within_ that general knowledge that are worth noting.

(Note: Not covered here due to how niche it is, but if you need anything more advanced than what is listed here then you may need something that more-or-less resembles `PDO::FETCH_UNIQUE|PDO::FETCH_GROUP|PDO::FETCH_ASSOC`.)

### Retrieve a single field (as a primitive data type)
Input:
```php
<?php
$pdo = new PDO(/* connection stuff */);
$sth = $pdo->prepare('SELECT name FROM users WHERE id = 1');
$sth->execute();
$result = $sth->fetchColumn();
```

Output:
```php
<?php
$result = 'John';
```

### Retreive all records - single column - flat array
Input:
```php
<?php
$pdo = new PDO(/* connection stuff */);
$sth = $pdo->prepare('SELECT id FROM users');
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_COLUMN);
```

Output:
```php
<?php
$result = [
	1,
	2,
	3,
	4
];
```

### Retrieve all records - two columns - key:value pair array
Input:
```php
<?php
$pdo = new PDO(/* connection stuff */);
$sth = $pdo->prepare('SELECT id, email FROM users');
$sth->execute();
$result = $sth->fetchAll(PDO::FETCH_KEY_PAIR);
```

Output:
```php
<?php
$result = [
	1	=>	'joe@example.com',
	2	=>	'john@example.com',
	3	=>	'jane@example.com',
	4	=>	'jesse@example.com'
];
