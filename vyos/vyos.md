# VyOS
> Note: All of this is written assuming that the installation was created by the Rackspace Cloud interface.

## Resources / Help
- [A Cheat Sheet I haven't gone through yet](https://github.com/bertvv/cheat-sheets/blob/master/src/VyOS.md)
- [Brocade - Vyatta Quick Start Guide v3.5R3](http://www.brocade.com/content/dam/common/documents/content-types/configuration-guide/vyatta-quickstart-3.5r3-v01.pdf)
- [Basic post-installation Vyatta configuration](https://rbgeek.wordpress.com/2013/05/06/vyatta-basic-configuration-after-installation)
- [Recent History of VyOS vs Brocade](http://dotbalm.org/brocade-missed-the-boat-with-vyatta/)

## General
Based on Debian. Vyatta was acquired in 2005 by Brocade and then disbanded in 2013 by the same company. It was always open source.

Upon disbandment, the open source software (OSS) community picked it up at Vyatta Router OS 6.6 where Brocade had left off and `merge` and `cherry-pick`ed their way between different stable branches and `master` to  create VyOS 6.7 and onwards as a 100% community-driven project. (As of this writing, the project is on `Vyatta RouterOS 6.7R12`...yeah, pretty old __now__, huh?)

## Commands
- This thing has _wonderful_ built-in documentation. Most questions can likelly be answered by hitting `Tab` twice or typing in `?`.
- **Operational Mode**
	- Description:
		- Basically, `bash`.
		- This is the default **Mode** when logging in. Running `exit` from inside of **Configureation Mode** will return to **Operational Mode**.
	- Commands:
		- `run` - Prefixing a **Configuration Mode** command with the `run` verb will execute that **Configuration Mode** command in a one-time, ad-hoc fashion rather than completely switching "modes".
- **Configuration Mode**
	- Description:
		- Vyatta's "special" shell, specifically designed around configuring itself.
		- Enter this mode by executing the following command: `configure`.
	- Commands:
		- `set`
			- Set a new value (or update an existing one)
		- `show`
			- Display the entire configuration tree from the next argument(s) and down
		- `delete`
			- Remove the target element and its children
		- `edit`
			- Enter a "scoped" view, as if to "step into" a function call in traditional languages. All operations performed while inside of this "namespace" are relative to itself, making it ideal for working with deeply-nested settings.
			- Running `exit` will return to the previous "scope"
			- Running `top` will explicitly return to the root-level configuration elements.
	- Nouns (there are many more):
		- `system`
			- `login`
				- `user`
			- `host-name`
			- `domain-name`
		- `interfaces`
		- `vpn`
			- `ipsec`
				- `site-to-site`
					- `peer`
						- `[user-defined]`
							- `local-address`
							- `tunnel`
								- `[user defined]`
									- `local`
										- `prefix [IP address/mask]`
									- `remote`
										- `prefix [IP address/mask]`
	- Syntax:
		- `set [set of keys which navigate the underlying "JSON" config file] [value]`
			- NOTE: The last segment of these space-delimited strings always seems to be the value.
		- `set foo bar baz qux`
			- This would theoretically create an entry in the config file which looks as follows:
				- `foo`
					- `bar`
						- `baz qux`
			- ...which is analagous to the following JSON (being worked into a Markdown list):
				- `"foo": {`
					- `"bar": {`
						- `"baz": "qux"`
					- `}`
				- `}`

## Logging In
`ssh vyatta@[IP address]` must be used for initial login rather than `ssh root@[IP]`. This is because `/etc/ssh/sshd_config`'s `PermitRootLogin no` is already set out of the box. Instead, the `vyatta` user is what gets the auto-generated password mentioned by the Rackspace Cloud interface.

## Users
### Root
Root is disabled by default in VyOS. To enable it, follow the steps for "Create" but for a user called `root`. For instance: `set system login user root authentication plaintext-password [PASSWORD]`.

### Management
- Create
	- `set system login user [USER] authentication plaintext-password '[PASSWORD]'`
		- _Note: Using double-quotes instead of single-quotes for the `[PASSWORD]` seems to cause problems._
- Read
	- `show system login user [USER]`
- Update
	- Password
		- See "Create"
	- Privileges
		- `set system login user [USER] level [LEVEL]`
			- Example: `set system login user robr3rd level admin`
- Delete
	- `delete system login user [USER]`

## Web GUI
In addition to the CLI, there is also a Web GUI. If it is disabled by default (unlike Rackspace Cloud), then it can theoretically be enabled in **Configuration Mode** with `set service https` followed by a `commit`.

## Setup / Best Practices
- Create a user for yourself
	- See "Users" --> " Management" --> "Create"
- Delete the default user
	- See "Users" --> " Management" --> "Delete", using `vyatta` as the value for `[USER]`
