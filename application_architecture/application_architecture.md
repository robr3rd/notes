# Application Architecture

## MVC
- M = Model
- V = View
- C = Controller

...the stuff that everybody knows

### MVC-L / MVC(L)
The same as "MVC", but with one addition:

- L = Language

> Introduction to MVC-L
> Getting started with opencart--
> OpenCart is an excellent platform for developers looking to understand PHP web frameworks in general. It is one of the easiest to follow MVC structured applications available. OpenCart allows you to learn the MVC framework while giving you access to the familiar PHP, mySQL and HTML technologies on which it is built. This guide will assume a basic understanding of HTML, CSS, Javascript, PHP (including classes and inheritance), and mySQL, and will describe how these are used in the OpenCart system.
> MVC(L)
> OpenCart is designed to follow an MVC design pattern. The components of MVC (Model View Controller) can be broken down as follows.
> _M_ - Model
> This is where you will interact directly with your database, pulling data out and restructuring it to a format that is suitable for your frontend. This will usually mainly consist of DB queries, and little more. If you are used to writing mySQL queries, you will enjoy the way OpenCart provides access to continue to do just that. OpenCart does not use an ORM, but allows you to write direct database queries.
> _V_ - View
> This is the display side of the MVC pattern. The idea of the _M_ and _C_ is to pull as much logic out of the view as possible, meaning simpler templates. In order to redesign your whole store, you simply modify the _View_ component, the _M_, _C_ and _L_ would remain the same. The view files in OpenCart have the `.tpl` suffix.
> _C_ - Controller
> This is where you will pull together the data from the Model, any config settings saved with your install or modules, and then render it by choosing the appropriate View file(s).
> _L_ - Language
> OpenCart extends _MVC_ to _MVCL_, providing an easy way of separating language specific information for internationalization. You can use language files to store any text like headings, titles, button text, etc., so that you only need to adjust one file per language to provide translations of your store.
>
> - [source](https://www.facebook.com/naive123/posts/653335378094129)

Official documentation: http://docs.opencart.com/developer/module/

Also see "mvcl.png" image (from the official documentation) in this same repo.
