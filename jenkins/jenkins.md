# Jenkins

## CSRF / Crumb Data
If you are trying to authenticate using (for instance) IntelliJ IDEA or its ilk (such as PhpStorm), then you'll need the CSRF token for your Jenkins installation (if that feature is enabled).

To retrieve that token, simply login and go to `[host]/crumbIssuer/api/xml?tree=crumb` and the crumb will be printed to the screen.
