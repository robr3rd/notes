# JavaScript



## Legacy Support via Babel
Write ES2015-compliant code, compile with [Babel](https://babeljs.io) into JavaScript that runs on current mainstream browsers, and "live in the future" while still supporting the past without having to think about it.

Tip: Check out their Presets, which tells the Babel compiler what your application needs to support.



## Style Guide
- [Airbnb](https://github.com/airbnb/javascript)



## Testing
- [Jest](http://facebook.github.io/jest/) (Unit Tests)
- [Jasmine](https://jasmine.github.io/) (Behavior-Driven Development)
