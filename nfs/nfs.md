# NFS (Network Filesystem)



## Context/Preface/General
There are 4 main options:

- CIFS/SMB (Samba): this is a Windows-land solution
- NFS: this is a UNIX-land solution
- WebDAV: works with all but client support is spotty
- SFTP: FTP via SSH



## Setup
Let's pretend that our NFS server has a LAN IP of 10.1.2.10 and our client is at 10.1.2.74


### Server
TL;DR: Put things on your real filesystem, then make as many bind-mounts for your NFS filesystem as you need, essentially creating a "virtual filesystem" built from bind-mounts.

```shell
# Install tools
sudo pacman -S nfs-utils

# Prepare filesystem structure
sudo mkdir /srv/nfs
sudo mkdir /srv/nfs/<your-share>
sudo vim /etc/fstab
# /mnt/<mountpoint>/<directorypath> /srv/nfs/<your-share>   none    bind,nofail 0   0

# Test/confirm filesystem preparations
sudo mount -a
sudo ls /srv/nfs/<your-share>

# Configure NFS
sudo vim /etc/exports
# /srv/nfs  10.1.2.0/24(rw,sync,crossmnt,fsid=root,no_subtree_check)
# /srv/nfs/<your-share>   10.1.2.0/24(rw,sync,no_subtree_check)

# Start/Enable the NFS server
sudo systemctl start nfs-server
sudo systemctl enable nfs-ser

# Apply changes
sudo exportfs -arv

# Test
sudo mkdir /mnt/nfstest
sudo mount -t nfs -o vers=4 10.1.2.10:/<your-share> /mnt/nfstest
sudo ls /mnt/nfstest  # if it has what you expect...SUCCESS!
sudo umount /mnt/nfstest  # unmount
sudo rmdir /mnt/nfstest  # finish cleaning up the test
```

Notes about `/etc/exports`:

```shell
# ...
# [default comment block]
# ...
#
# robr3rd custom
# Configuration Notes:
# - Permissions
#   - rw = read/write
#   - ro = read-only
# - Write method
#   - sync vs. async ... async is faster but data could be lost if shutdown while the 'write' is in MEM
#   - sync = write to disk immediately (lock the file & make things wait if you must)
#   - async = write to RAM first and persist to disk 'when you get the chance'
# - Visibility
#   - crossmnt (on parent) = make all children visible (note: this forces 'no_hide' on all children)
#   - no_hide (on child) = make THIS child visible
# - Filesystem ID (fsid)
#   - Each NFS 'root' should have a fsid value; the defaul is `fsid=root` or `fsid=0`
# - Sub-tree check
#   - TL;DR: Only relevant when the NFS root is a SUBDIRECTORY of a filesystem and not the entire filesystem, itself, it's a security vs. reliability vs. speed trade-off.
#   - subtree_check = verify that everything is both within the filesystem AND within the 'exports' rules
#   - no_subtree_check = don't both
#
# Syntax
# host_path	client_CIDR(configuration)
# /srv/nfs  10.1.2.0/24(rw,sync,crossmnt,fsid=root,no_subtree_check)
# /srv/nfs/<your-share>   10.1.2.0/24(rw,sync,no_subtree_check)
```


### Client
```shell
# Setup
sudo mkdir /mnt/<my-share>  # make a home (local mountpoint)

# Manual test
sudo mount -t nfs -o vers=4 10.1.2.10:/<your-share> /mnt/<my-share>  # manually mount it
sudo ls /mnt/<my-share>  # if it has what you expect...SUCCESS!
sudo umount /mnt/<my-share>  # manually unmount out test mount

# Automate to happen on boot
sudo vim /etc/fstab
# 10.1.2.10:/<your-share>  /mnt/<my-share> nfs defaults,timeo=900,retrans=5,_netdev    0   0

# Automation test
sudo mount -a
sudo ls /mnt/<my-share>  # if it has what you expect...SUCCESS!
```
