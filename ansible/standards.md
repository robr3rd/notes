# Coding Standards
Aside from the standards set forth by the YAML language, there aren't any kind of Coding Standards for Ansible (but there *are* various [best practices in the official docs](http://docs.ansible.com/ansible/playbooks_best_practices.html)).

That being said, there _is_ [`ansible-lint`](https://github.com/willthames/ansible-lint).

As such, here I'll outline some various naming conventions that I've found useful and the least problematic.



## Capitalization
### Handlers
- Handlers MUST be lower-case
	- Examples
		- Apache
			- `restart httpd` <-- Right!
			- `Restart httpd` <-- Wrong!
			- `Restart Httpd` <-- Wrong!
			- `Restart HTTPD` <-- Wrong!
		- iptables
			- `restart iptables` <-- Right!
			- `Restart IPTables` <-- Wrong!
			- `Restart IPtables` <-- Wrong!
			- `Restart IpTables` <-- Wrong!
			- `Restart Iptables` <-- Wrong!
			- `Restart ipTables` <-- Wrong!
		- cURL (note: I realize that this is not a service, but it demonstrates my point excellently)
			- `restart curl` <-- Right!
			- `Restart cURL` <-- Wrong! (note: Although it *does* follow the official nomenclature if you ask 3 developers how it's cased you'll get 5 answers)
			- `Restart Curl` <-- Wrong!
			- `Restart cUrl` <-- Wrong!
	- Reason
		- Unix projects have a tendency to have unpredictable naming conventions when it comes to capitalization especially (e.g. `gEdit` and perhaps more notoriously `cURL`).
		- In the spirit of consistency (especially in multi-dev environments) it's just easier to make all `handlers` lower-cased and call it a day.
