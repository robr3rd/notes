# Ansible
Ansible is a server configuration management & provisioning tool.

## Introduction
- https://semaphoreci.com/community/tutorials/introduction-to-ansible



## Syntax
Aside from the Ansible official docs' [Intro to YAML Syntax](http://docs.ansible.com/ansible/YAMLSyntax.html), this is pretty well-summarized on [www.json2yaml.com's home page](https://www.json2yaml.com) and (further) on the same site's [excellent _quick_ overview](https://www.json2yaml.com/yaml-vs-json) of a comparison of the two and when to use each.


### Basic Syntax Check
- `ansible-playbook [role/tests/inventory] [role/tests/test.yml] --syntax-check` - This will fail if there are any errors


### Linting
- Use [`ansible-lint`](https://github.com/willthames/ansible-lint)


### Tricky Syntax
####Register a variable from a loop and reference back to it associatively
Results are technically numerically-indexed dicts, so it's not obvious how to emulate associative array functionality in them.

The key is that when a variable is registered, Ansible appears to store anything present/used in the creation of that variable (such as `with_*`) in it alongside the actual output.

For instance, given the below:

```yaml
---
- vars:
	- apps:
			paths:
				app1: /path/to/foo/app1
				app2: /path/to/bar/app2
- tasks:
	- name: Check the existence of application directories
		stat:
			path: "/var/www/vhosts/{{ item.value }}"
		with_dict: "{{ apps.paths }}"
		register: stat_result

	- name: Copy contents for all VirtualHost sites
		copy:
			src: "vhosts/{{ item.item.value }}"
			dest: "/var/www/vhosts/{{ item.item.value }}"
		with_items: "{{ stat_result.results }}"
		when: item.stat.exists == False
		register: stat_result.results
 ```

...the value of `stat_result` would be:

```json
"stat_result": {
	"changed": false,
	"msg": "All items completed",
	"results": [
		{
			"changed": false,
			"invocation": {
				"module_args": {
					"...": "..."
				},
				"module_name": "stat"
			},
			"item": {
				"key": "app1",
				"value": "/path/to/foo/app1"
			}
		},
		{
			"changed": false,
			"invocation": {
				"module_args": {
					"...": "..."
				},
				"module_name": "stat"
			},
			"item": {
				"key": "app2",
				"value": "/path/to/bar/app2"
			}
		}
	],
	"stat": {
		"exists": true,
		"isdir": true,
		"path": "...so on so forth for `stat` results"
	}
}
```
