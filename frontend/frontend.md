# Frontend

## Progressive Web Apps
- https://developers.google.com/web/progressive-web-apps/
- https://scotch.io/tutorials/the-ultimate-guide-to-progressive-web-applications



## BEM / naming convention
```sass
// Syntax: `.[context-]component[--state] {}`

// All components are self-contained. These may be 'simple' (such as a content component)
// or 'complex' (such as a structural component CONTAINING one or more 'simple' components).
//
// In summary, a "simple component" MAY or MAY NOT be a part of a "complex component", but
// a "complex component" MUST contain one or more "simple components".


// Naming convention
.block_name-element_name--modifier {} // simple

// multiple simples
.button {} // simple
.link {} // simple


// Accordion
.accordion {} // complex
.accordion-item {} // simple
.accordion-title {} // simple
.accordion-content {} // simple


// Navigation
.nav {} // complex
.nav-item {} // simple
.nav-item--isActive {} // simple (+state)


// Header
.header { // complex
	background-color: gray;

	.logo {} // simple
	.button {} // simple

	// complex
	.nav {}
}

.header-title {} // simple
```


## Event Propagation to sub-elements
This comes from [this article](https://css-tricks.com/slightly-careful-sub-elements-clickable-things), although I'll caution that its advice is not very good advice; however, the _comments section_ is **rich with advice**--specifically, [this comment](https://css-tricks.com/slightly-careful-sub-elements-clickable-things/#comment-1611278) (and [its sub-comment](https://css-tricks.com/slightly-careful-sub-elements-clickable-things/#comment-1611279)).

Here's the inline content of note for posterity:


> I never use non-interactable elements for listening to DOM events, so I have only a few to worry about:
>
```css
// prevent events propagating to children
a,
button,
label,
select {
  > * {
    pointer-events: none;
  }
}
```
>
> - Ivan Curic, 2017-08-22

> And while on the topic, the following prevents text selection when click-mashing interactable elements:
>
```css
button,
[role="button"],
label,
select {
  user-select: none;
}
```
>
> - Ivan Curic, 2017-08-22
