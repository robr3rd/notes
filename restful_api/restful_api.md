# RESTful API
There will be inevitable crossover with nearly every other "web" section. Crossover will be kept brief and to the point and a reference link should be used in the place of long-form explanations.


## Syntax
> URI = scheme "://" authority "/" path [ "?" query ] [ "#" fragment ]
> - [RFC 3986](https://www.ietf.org/rfc/rfc3986.txt)



## HTTP Methods
> `GET` - Retrieve resources
> `POST` - Create resources
> `PUT` - Update whole resources
> `PATCH` - Updates pieces of resources
> `DELETE` - Delete resources
>
> -Adapted from http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html



## HTTP Status Codes
> Here are some of the most used, in my humble opinion :)
>
> - 2xx - Success
> 	- 200 OK
> 	- 201 Created
> 	- 203 Partial Information
> 	- 204 No response
> - 3xx - Redirection
> 	- 301 Moved
> 	- 302 Found
> 	- 304 Not Modified
> - 4xx / 5xx - Error
> 	- 400 Bad Request
> 	- 401 Unauthorized
> 	- 402 Payment Required
> 	- 403 Forbidden
> 	- 404 Not Found
> 	- 500 Internal Server Error
> 	- 503 Service Unavailable
>
> -http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html



## Idempotency
> No matter how many times you call `GET` on the same resource, the response should always be the same and no change in application state should occur.
>
> Idempotent methods: `GET`, `PUT`, `OPTIONS`
> Non Idempotent methods: `POST`
>
> What about the method `DELETE`? If you `DELETE /accounts/4402278` twice...:
> 1. The accounts will not be deleted many times ... thinking this way it is idempotent
> 1. The second time the resource will not be found and should return a 404 Not found, this way it's not idempotent anymore
>
> -http://marcelo-cure.blogspot.com/2016/09/rest-anti-patterns.html



## 5 Basic REST API Design Guidelines
Adapting content from: http://blog.restcase.com/5-basic-rest-api-design-guidelines

### Resources (URIs)
#### Names and Verbs
Don't do `getUser(1234)`, `createUser(user)`, or `deleteAddress(1234)`; rather, use `GET /users/1234`, `POST /users (with JSON describing a user in the body)`, and `DELETE /addresses/1234`.

#### URI Case
Use `spinal-case` instead of `snake_case` or `StudlyCaps` (per RFC3986).

### HTTP methods
- GET - The GET method is used to retrieve information from the given server using a given URI. Requests using GET should only retrieve data and should have no other effect on the data.
- HEAD - Same as GET, but transfers the status line and header section only.
- POST - A POST request is used to send data to the server, for example, customer information, file upload, etc. using HTML forms.
- PUT - Replaces all current representations of the target resource with the uploaded content.
- DELETE - Removes all current representations of the target resource given by a URI.
- OPTIONS - Describes the communication options for the target resource.

### Query parameters
- Paging - Tip: pass in the page to show but not the page size when early on in development. Sticking to defaults makes life easier.
- Filtering - Remember: It's possible to filter on more than one attribute as well as apply more than one value to a single attribute.
- Sorting - "A sort parameter should contain the names of the attributes on which the sorting is performed, separated by a comma."
- Searching - "A search is a sub-resource of a collection. As such, its results will have a different format than the resources and the collection itself. This allows us to add suggestions, corrections and information related to the search.
Parameters are provided the same way as for a filter, through the query-string, but they are not necessarily exact values, and their syntax permits approximate matching."

### Summary
"REST is not something new, REST is a return to the Web the way it was before the age of the big application server, through its emphasis on the early Internet standards, URI and HTTP.

Resource modeling requires a careful consideration based on the business needs, technical considerations (clean design, maintainability, etc.) and cost-benefit analysis of various approaches discussed earlier so that the API design brings out the best API consumer interaction experience"



## 7 Rules for REST API URI Design
> source: http://blog.restcase.com/7-rules-for-rest-api-uri-design

### URIs
- Do: http://api.example.com/louvre/leonardo-da-vinci/mona-lisa
- Don't: http://api.example.com/68dd0-a9d3-11e0-9f1c-0800200c9a66

> The only thing you can use an identifier for is to refer to an object. When you are not dereferencing, you should not look at the contents of the URI string to gain other information.
> - Tim Berners-Lee

- Rule 1: A trailing forward slash should be redirected to non-trailing slash or fail. (Example: `http://api.canvas.com/shapes/` -> `http://api.canvas.com/shapes`)
- Rule 2: Use `/` for hierarchy
- Rule 3: Use hyphens (`-`) to improve readaibility of URIs
- Rule 4: Don't use underscores (`_`). (instead, see #3)
- Rule 5: Prefer lowercase letters. `http://api.example.com/my-folder/my-doc` == `HTTP://API.EXAMPLE.COM/my-folder/my-doc`, but not `http://api.example.com/My-Folder/my-doc`
- Rule 6: Don't include file extensions in the URI; instead, APIs should rely on the media type, as communicated through the `Content-Type` header when determining how to process the body's content. Similarly, "REST API clients should be encouraged to utilize HTTP’s provided format selection mechanism, the `Accept` request header[, although t]o enable simple links and easy debugging, a REST API may support media type selection via a query parameter."
- Rule 7: Use plural endpoints rather than singular (e.g. `/students` and `/students/`). Relations should look like: `http://api.college.com/students/3248234/courses` and `http://api.college.com/students/3248234/courses/physics`.
