# Git - Snippets

## A Huge list of nice snippets
- https://csswizardry.com/2017/05/little-things-i-like-to-do-with-git/



## "Where am I?"
- Branch/Tag only (no detached HEAD)
	- Input: `git symbolic-ref --short HEAD`
	- Output examples:
		- `master`
		- `some-branch`
		- `fatal: ref HEAD is not a symbolic ref` (if in a detached HEAD state)
- Branch/Tag first + fallback to detached HEAD
	- Input: `git rev-parse --abbrev-ref HEAD`
	- Output examples:
		- `master`
		- `some-branch`
		- `HEAD` (if in a detached HEAD state)
- Precise, relative location
	- Input: `git name-rev --name-only HEAD`
	- Output examples:
		- `master^0`
		- `master~1` (if in a detached HEAD state, show position relative to nearest "anchor")
		- `some-branch^0`
		- `tags/x.y.z^0` (if in a detached HEAD state due to checking out a tag)



## Removing accidental whitespace (from a Pull Request)
If the commit was the most recent commit in the branch, then obviously `git reset --hard HEAD^1` is simpler and much more straight-forward; however, if your situation is more complex than that -- with errant whitespace changes strewn across multiple commits, but intermingled with REAL work that you would still like to keep -- then an appropriately more complex solution is necessary. Specifically, something like the following:

```bash
git reset --soft HEAD~1 # checkout the previous commit (and move the `diff` to the current working branch)
git diff -w origin/master > /tmp/commit.patch # `-w` means "ignore whitespace"...this is comparing what we checked out vs what's in our working branch
git reset --hard HEAD # blow away our working branch (which still includes whitespace)
# Note: We are now prior to when we foolishly made whitespace changes
# Now make the whitespace changes (e.g. start saving files with EditorConfig or apply other similar rulesets) and `commit` it all as a "whitespace changes" commit
git apply /tmp/commit.patch # apply the non-whitespace changes that we identified from our `git diff -w` earlier
git add . -A
git commit -v -a # the `-v` (verbose...shows the diff of what you'll be committing) is optional and only exists to demonstrate and verify that this process worked
# [copy/paste your old commit message]
git push --force # because we just rewrote this branch's history
rm /tmp/commit.patch # clean up
```



## What changed in that location between now and then?
`git diff [--flags] ["from" tag, branch, or commit hash] HEAD [filepath scope of the diff]`

For `[--flags]`, see these potential candidates to help tailor the output to your needs:

- `--shortstat` shows total number of files changed along with total additions and total deletions
	- Example: `1 file changed, 5 insertions(+), 6 deletions(-)`
- `--short` shows list of files changed with summary of additions, deletions, and changes per file + the output of `--shortstat` (similar to `git status`)
	- Example: `path/to/file.extension | 11 +++++------` + `--shortstat`
- `--minimal` provides a more traditional `diff` appearance much like a `patch` file



## Fixing commit metadata
### Retroactively change an email address                     
```bash
git filter-branch --env-filter '
oldname="Foo Bar Johnson"
oldemail="user@personal-example.com"
newname="Foo Bar Johnson"
newemail="user@employer.com"
if [ "$GIT_AUTHOR_EMAIL" = "$oldemail" ]; then GIT_AUTHOR_EMAIL="$newemail"; fi
if [ "$GIT_COMMITTER_EMAIL" = "$oldemail" ]; then GIT_COMMITTER_EMAIL="$newemai$
if [ "$GIT_AUTHOR_NAME" = "$oldname" ]; then GIT_AUTHOR_NAME="$newname"; fi
if [ "$GIT_COMMITTER_NAME" = "$oldname" ]; then GIT_COMMITTER_NAME="$newname"; $
' HEAD
```


### Retroactively change timestamp of commit
```bash
git filter-branch --env-filter \
'if [ $GIT_COMMIT = abcdef123456 ]
 then
     export GIT_AUTHOR_DATE="Fri Dec 6 15:04:50 2019 -0500"
     export GIT_COMMITTER_DATE="Fri Dec 6 15:04:50 2019 -0500"
 fi'
```



## Convenience Aliases (upstream)
The CLI API was simplified a bit in [v2.23.0](https://github.com/git/git/releases/tag/v2.23.0) for some of the most commonly performed actions. ([Release Notes](https://github.com/git/git/blob/master/Documentation/RelNotes/2.23.0.txt))

### Overview
A typical workflow can go from this...:
```bash
git checkout review-branch
echo 'hello world' > foo.txt  # "whoops, that was an accident"
git checkout -- foo.txt
```

...to this (if desired):
```bash
git switch review-branch
echo 'hello world' > foo.txt  # "whoops, that was an accident"
git restore foo.txt
```

It's hardly mind-blowing, BUT it is MUCH more intuitive--especially for users that are new to VCS or migrating from Subversion to Git since `svn checkout==git clone` which can make transitioning harder.

Overall, this small syntax change should make day-to-day usage of common tasks just that much easier.


### Functionality Comparison (old vs. new)
- `git switch <branch>`
	- formerly: `git checkout <commitish>`
	- examples:
		- branch
			- `git switch my-branch` <-- **works**
			- `git checkout my-branch` <-- **works**
		- tag
			- `git switch 3.0.0` <-- **fails** (`a branch is expected, got tag '3.0.0'`)
			- `git checkout 3.0.0` <-- **works**
		- commit
			- `git switch 9a62b722` <-- **fails** (`a branch is expected, got commit '9a62b722'`)
			- `git checkout 9a62b722` <-- **works** (detached `HEAD` at `9a62b722`)
- `git restore <filepath>`
	- formerly: `git checkout <source> -- <filename>`
	- examples:
		- discard changes in the working branch
			- `git restore foo.txt` <-- **works**
			- `git checkout HEAD -- foo.txt` <-- **works**
				- short-hand: `git checkout -- foo.txt`
		- replace a file with a different version of itself
			- `git restore origin/my-branch foo.txt` <-- **fails** (file not found "origin/my-branch")
			- `git checkout origin/my-branch -- foo.txt` <-- **works**
