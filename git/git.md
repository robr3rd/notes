# Git


## History searches
To grep (search) Git's history for _content_--i.e., code changes vs. metadata like commit messages--use the `pickaxe` (`-S`) option of `git log`.

```sh
git log -- path/to/file.ext               # show the history of the given path
git log -SFooBar                          # find when 'FooBar' appeared in a diff 
git log -SFooBar -- relevant_file_path    # find when 'FooBar' appeared in a diff for a file within the provided path
git log -S"Foo(Bar|Qux)" --pickaxe-regex -- relevant_file_path  # same thing, but with a POSIX regular expression
git log -SFooBar --since=2020-03-01 --until=2020-06-30 -- relevant_file_path  # [...] and within the given time frame
```


## Git Hooks
- Excellent general overview of all hooks including things like order of execution and which ones are local vs remote: https://scotch.io/tutorials/using-git-hooks-in-your-development-workflow
- In-depth dive on pre-commit hooks from Yelp: http://pre-commit.com/



## Submodules
To pull only a epcific path from within a submodule, use the following template in your project's `.gitmodules` file:

```ini
[submodule "magento/magento2"]
	path = PHP_CodeSniffer/magento2
	url = https://github.com/magento/magento2.git
```

In this example, we are calling the `magento/magento2` repository, but only pulling the `path`. The end result of this is that we're pulling `github.com/magento/magento2.git/PHP_CodeSniffer/magento2`.



## Custom Commands
> You can extend and customize git through command aliases. You can also add entirely new commands.
> Place any executable with a name like git-squash in your PATH and git will automatically make it available as a subcommand.
> You can then call it just like any other git command: `git squash 3`
> The git executable places precedence with it's own commands in `ls $(git --exec-path)`, the `GITEXECPATH` environment variable, and the `$(gitexecdir)` from the git Makefile during build/install time, so make your command name is unique. That's how projects like git-flow extend git.
> Turns out git provides a library of shell functions expressly for this purpose, at `$(git --exec-path)/git-sh-setup`. Use it in your own shell scripts like: `source "$(git --exec-path)/git-sh-setup"`
> After that you'll have access to a handful of shell functions that perform useful actions and sanity checks or give you access to git information such as handy `usage` and `die` methods.
> In fact many of the executables that ship with git are just shell scripts, such as `git bisect` and even `git rebase`.
> - https://coderwall.com/p/bt93ia/extend-git-with-custom-commands


### Pre-built Custom Command Libraries
- http://www.git-town.com/
	- This would be a good base to fork & extend, perhaps... :)



## Administration
### Get current size of repository 
`git count-objects -v` will give you the size of various components of your repository (in KB).

Here's a quick one-liner to help make this output more readable and helpful by warning of sizes >1GB and converting KB to MB on an as-needed basis:

```sh
git count-objects -v | awk '{size=$2;printf "%s%s%.2f%s%s\n",$1," ",(size>1024?size/1024:size),(size>1024?" MB":" KB"),(size>(1024*1024)?" <=== Warning! Size > 1GB":"")}'
```
