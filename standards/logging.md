# Standards - Logging

## PSR-3 (PHP-FIG Standards)
> ```
> # Psr\Log\LogLevel
> <?php
> 
> namespace Psr\Log;
> 
> /**
>  * Describes log levels
>  */
> class LogLevel
> {
>     const EMERGENCY = 'emergency';
>     const ALERT     = 'alert';
>     const CRITICAL  = 'critical';
>     const ERROR     = 'error';
>     const WARNING   = 'warning';
>     const NOTICE    = 'notice';
>     const INFO      = 'info';
>     const DEBUG     = 'debug';
> }
> ```
> 
> ```
> # Psr\Log\LoggerInterface
> <?php
> 
> namespace Psr\Log;
> 
> /**
>  * Describes a logger instance
>  *
>  * The message MUST be a string or object implementing __toString().
>  *
>  * The message MAY contain placeholders in the form: {foo} where foo
>  * will be replaced by the context data in key "foo".
>  *
>  * The context array can contain arbitrary data, the only assumption that
>  * can be made by implementors is that if an Exception instance is given
>  * to produce a stack trace, it MUST be in a key named "exception".
>  *
>  * See https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-3-logger-interface.md
>  * for the full interface specification.
>  */
> interface LoggerInterface
> {
>     /**
>      * System is unusable.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function emergency($message, array $context = array());
> 
>     /**
>      * Action must be taken immediately.
>      *
>      * Example: Entire website down, database unavailable, etc. This should
>      * trigger the SMS alerts and wake you up.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function alert($message, array $context = array());
> 
>     /**
>      * Critical conditions.
>      *
>      * Example: Application component unavailable, unexpected exception.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function critical($message, array $context = array());
> 
>     /**
>      * Runtime errors that do not require immediate action but should typically
>      * be logged and monitored.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function error($message, array $context = array());
> 
>     /**
>      * Exceptional occurrences that are not errors.
>      *
>      * Example: Use of deprecated APIs, poor use of an API, undesirable things
>      * that are not necessarily wrong.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function warning($message, array $context = array());
> 
>     /**
>      * Normal but significant events.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function notice($message, array $context = array());
> 
>     /**
>      * Interesting events.
>      *
>      * Example: User logs in, SQL logs.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function info($message, array $context = array());
> 
>     /**
>      * Detailed debug information.
>      *
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function debug($message, array $context = array());
> 
>     /**
>      * Logs with an arbitrary level.
>      *
>      * @param mixed $level
>      * @param string $message
>      * @param array $context
>      * @return null
>      */
>     public function log($level, $message, array $context = array());
> }
> ```
> http://www.php-fig.org/psr/psr-3/


## RFC-5424
> https://tools.ietf.org/html/rfc5424#section-6.2.1
>
> 6.2.1.  PRI
>    The PRI part MUST have three, four, or five characters and will be
>    bound with angle brackets as the first and last characters.  The PRI
>    part starts with a leading "<" ('less-than' character, %d60),
>    followed by a number, which is followed by a ">" ('greater-than'
>    character, %d62).  The number contained within these angle brackets
>    is known as the Priority value (PRIVAL) and represents both the
>    Facility and Severity.  The Priority value consists of one, two, or
>    three decimal integers (ABNF DIGITS) using values of %d48 (for "0")
>    through %d57 (for "9").
>
>    Facility and Severity values are not normative but often used.  They
>    are described in the following tables for purely informational
>    purposes.  Facility values MUST be in the range of 0 to 23 inclusive.
>
>           Numerical             Facility
>              Code
>
>               0             kernel messages
>               1             user-level messages
>               2             mail system
>               3             system daemons
>               4             security/authorization messages
>               5             messages generated internally by syslogd
>               6             line printer subsystem
>               7             network news subsystem
>               8             UUCP subsystem
>               9             clock daemon
>              10             security/authorization messages
>              11             FTP daemon
>              12             NTP subsystem
>              13             log audit
>              14             log alert
>              15             clock daemon (note 2)
>              16             local use 0  (local0)
>              17             local use 1  (local1)
>              18             local use 2  (local2)
>              19             local use 3  (local3)
>              20             local use 4  (local4)
>              21             local use 5  (local5)
>              22             local use 6  (local6)
>              23             local use 7  (local7)
>
>               Table 1.  Syslog Message Facilities
>
>    Each message Priority also has a decimal Severity level indicator.
>    These are described in the following table along with their numerical
>    values.  Severity values MUST be in the range of 0 to 7 inclusive.
>            Numerical         Severity
>              Code
>
>               0       Emergency: system is unusable
>               1       Alert: action must be taken immediately
>               2       Critical: critical conditions
>               3       Error: error conditions
>               4       Warning: warning conditions
>               5       Notice: normal but significant condition
>               6       Informational: informational messages
>               7       Debug: debug-level messages
>
>               Table 2. Syslog Message Severities
>
>    The Priority value is calculated by first multiplying the Facility
>    number by 8 and then adding the numerical value of the Severity.  For
>    example, a kernel message (Facility=0) with a Severity of Emergency
>    (Severity=0) would have a Priority value of 0.  Also, a "local use 4"
>    message (Facility=20) with a Severity of Notice (Severity=5) would
>    have a Priority value of 165.  In the PRI of a syslog message, these
>    values would be placed between the angle brackets as <0> and <165>
>    respectively.  The only time a value of "0" follows the "<" is for
