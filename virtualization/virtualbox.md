# Virtualization - VirtualBox

## VirtualBox & Cisco AnyConnect: A tale of two network resolvers
### About
If in an environment with both VirtualBox and AnyConnect in use, problems are likely to arise with regards to yor host's ability to resolve 
network targets (VMs) via the VirtualBox network driver/module.

In general, it seems that this happens when--from a fresh boot--AnyConnect is used _prior to using VirtualBox_; inversely, if a VirtualBox guest is created _prior to using AnyConnect_ then the issue is unlikely to arise.


### Symptoms
- Host is unable to resolve guests' IP addresses or host mappings.
    - Example:
        - Guest IP Address: `192.168.50.151`
        - "Domain name" resolution: `/etc/hosts` --> `192.168.50.151 example.test`
        - ...browser unable to load `example.test` or `192.168.50.151`.
- Able to workaround with port forwarding.
    - Example:
        - Port Forwarding: `8051`-->`51`
        - ...browser capable of loading `localhost:8051`.


### Fix
#### Option 1
Restart host machine.  This will disconnect from all networking interfaces (obviously) and reload all VirtualBox modules on boot.

#### Option 2
- Disconnect from AnyConnect
- Stop all VirtualBox VMs
- Force a reload of all VirtualBox modules/drivers (which includes its network driver)
    - macOS:
        1. `sudo /Library/Application\ Support/VirtualBox/LaunchDaemons/VirtualBoxStartup.sh restart`
    - Windows:
        1. `cd c:\Program Files\Oracle\Virtualbox`
        1. `VBoxManage.exe modifyvm "your_Linux_VM_guest_name_here" --natdnshostresolver1 on`


### Resources
 - https://community.cisco.com/t5/vpn-and-anyconnect/dns-issues-on-cisco-anyconnect-client/td-p/2938979
    - _"I have spoken to Cisco and apparently this is a change of behaviour (meaning it will not be fixed)."_
 - http://www.robertparten.com/virtualbox-linux-guest-dns-problems-anyconnect/